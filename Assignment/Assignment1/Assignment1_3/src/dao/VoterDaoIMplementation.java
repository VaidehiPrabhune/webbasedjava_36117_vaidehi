package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pojo.Voter;
import Utils.DBUtils;;

public class VoterDaoIMplementation implements Closeable,IVoterDao{
	Connection connection = null;
	Statement statement = null;
	public VoterDaoIMplementation() throws Exception{
		 this.connection = DBUtils.getConnection();
		 this.statement = this.connection.createStatement();
	}
	public Voter validateUser(String email,String password) throws Exception
	{
		Voter voter=new Voter();
		String sql = "SELECT * FROM voters where email="+email+"and"+"password="+password+"";
		try( ResultSet rs = this.statement.executeQuery(sql) ){
			while( rs.next())
				voter.setId(rs.getInt("id"));
				voter.setName(rs.getString("name"));
				voter.setEmail(rs.getString("email"));
				voter.setPassword(rs.getString("password"));
				voter.setRole(rs.getString("role"));
				voter.setStatus(rs.getBoolean("status"));
		}
		if(voter.getId() == 0)
		{
			return null;
		}
		else 
			return voter;				
	}
	public int insert(Voter voter) throws Exception{
		String sql = "INSERT INTO voters VALUES("+voter.getId()+",'"+voter.getName()+"','"+voter.getEmail()+"','"+voter.getPassword()+"',"+voter.getStatus()+"',"+voter.getRole()+")";
		return this.statement.executeUpdate(sql);
	}
	public int update(int id, boolean status) throws Exception{
		String sql = "UPDATE voters SET status="+status+" WHERE id="+id+"";
		return this.statement.executeUpdate(sql);
	}
	public int delete(int id) throws Exception{
		String sql = "DELETE FROM voters WHERE id="+id+"";
		return this.statement.executeUpdate(sql);
	}
	public List<Voter> getVoters( )throws Exception{
		String sql = "SELECT * FROM voters";
		List<Voter> voters = new ArrayList<Voter>();
		try( ResultSet rs = this.statement.executeQuery(sql) ){
			while( rs.next())
				voters.add( new Voter(rs.getInt("id"),rs.getString("name"),rs.getString("email"),rs.getString("password"),rs.getString("role"),rs.getBoolean("status")) );
		}
		return voters;
	}
	
	@Override
	public void close() throws IOException {
		try {
			connection.close();
		} catch (SQLException cause) {
			throw new IOException(cause);
		}
	}
}