package Utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBUtils {
	private static Properties p;
	static {
		try {
			FileInputStream inputStream = new FileInputStream("Config.properties");
			p = new Properties();
			p.load(inputStream);
			Class.forName(p.getProperty("DRIVER"));
		} catch (Exception cause) {
			throw new RuntimeException(cause);
		} 
	}
	public static Connection getConnection( ) throws SQLException{
		return DriverManager.getConnection(p.getProperty("URL"), p.getProperty("USER"), p.getProperty("PASSWORD"));		
	}
}
