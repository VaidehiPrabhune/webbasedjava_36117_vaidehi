package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ClientServlet
 */
@WebServlet(description = "Client data", urlPatterns = { "/clientdata" })
public class ClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	@Override
	public void destroy() {
		System.out.println("in destroy "+Thread.currentThread());
	}

	@Override
	public void init() throws ServletException {
		System.out.println("in init of "+getClass().getName()+Thread.currentThread());
	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set resp content type
				response.setContentType("text/html");
				//open PW to send resp from servlet ---> clnt
				try(PrintWriter pw=response.getWriter())
				{
					pw.print("<h5>User Name 	   : "+request.getParameter("f1")+"</h5>");
					pw.print("<h5>Favorite Colors  : "+request.getParameterValues("clr")+"</h5>");
					pw.print("<h5>Browser          : "+request.getParameter("browser")+"</h5>");
					pw.print("<h5>City			   : "+request.getParameter("myselect")+"</h5>");
					pw.print("<h5>About Yourself  : "+request.getParameterValues("info")+"</h5>");
					
				}
	}

}
