package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FirstServlet
 */
//WC : @ dep time 
//entry=mapping=key n value pair
//key : /hello
//value : pages.FirstServlet
@WebServlet(value="/hello",loadOnStartup = 1)
public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set resp content type(HTTP resp hdr) : meant for clnt browser
		System.out.println("in do-get "+Thread.currentThread());
		response.setContentType("text/html"); //to know client data is in html context
		//open pw : to send response from server ----> client
		try(PrintWriter pw=response.getWriter())
		{
			pw.print("<h5>welcome 2 servlets "+new Date()+"</h5>");
		}
		//resp pkt : status code(200) | header/s(cont type , cont len) | dynamic contents(body) : welcome msg
	}

}
