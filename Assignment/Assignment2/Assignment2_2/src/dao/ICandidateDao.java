package dao;

import java.util.List;

import pojo.Candidate;

public interface ICandidateDao {
	public List<Candidate> listCandidates() throws Exception;
}
