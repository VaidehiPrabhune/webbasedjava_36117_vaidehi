package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import static Utils.DBUtils.getConnection;

import pojo.Voter;


public class VoterDaoIMplementation implements IVoterDao{
	//data members:instance variables
	private Connection connection = null;
	private PreparedStatement pst1 = null;
	PreparedStatement stmtInsert;
	PreparedStatement stmtUpdate;
	PreparedStatement stmtDelete;
	PreparedStatement stmtSelect;
	
	public VoterDaoIMplementation() throws Exception{
		 this.connection = getConnection();
		 this.pst1 = connection.prepareStatement("select * from voters where email=? and password=?");
		 this.stmtInsert = this.connection.prepareStatement("INSERT INTO voters VALUES(?,?,?,?,?,?)");
		 this.stmtUpdate = this.connection.prepareStatement("UPDATE voters SET status=? WHERE id=?");
		 this.stmtDelete = this.connection.prepareStatement("DELETE FROM voters WHERE id=?");
		 this.stmtSelect = this.connection.prepareStatement("SELECT * FROM voters");
		 System.out.println("voter dao created.....");
	}
	
	public Voter validateUser(String email,String password) throws Exception
	{
		pst1.setString(1, email);
		pst1.setString(2, password);
		try(ResultSet rst=pst1.executeQuery())
		{
			if(rst.next())
				return new Voter(rst.getInt(1), rst.getString(2),email, password,rst.getBoolean(5),rst.getString(6));
			else
				return null;
		}
					
	}
	public int insert(Voter voter) throws Exception{
		this.stmtInsert.setInt(1, voter.getId());
		this.stmtInsert.setString(2, voter.getName());
		this.stmtInsert.setString(3, voter.getEmail());
		this.stmtInsert.setString(4, voter.getPassword());
		this.stmtInsert.setBoolean(5, voter.getStatus());
		this.stmtInsert.setString(6, voter.getRole());
		return this.stmtInsert.executeUpdate();
	}
	public int update(int id, boolean status) throws Exception{
		this.stmtUpdate.setInt(1, id);
		this.stmtUpdate.setBoolean(2, status);
		return this.stmtUpdate.executeUpdate();
	}
	public int delete(int id) throws Exception{
		this.stmtDelete.setInt(1, id);
		return this.stmtUpdate.executeUpdate();
	}
	public List<Voter> getVoters( )throws Exception{
		List<Voter> voters = new ArrayList<Voter>();
		try( ResultSet rs = this.stmtInsert.executeQuery() ){
			while( rs.next())
			{
				Voter voter	=new Voter(rs.getInt("id"),rs.getString("name"),rs.getString("email"),rs.getString("password"),rs.getBoolean("status"),rs.getString("role")) ;
				voters.add(voter);
			}
		}
		return voters;
	}
	
	//Cleaning up of DB resources
	public void cleanUp() throws Exception{
		if(pst1 != null)
			pst1.close();
		if(connection != null)
			connection.close();
	}
}