package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class VoterServlet
 */
@WebServlet("/voterStatus")
public class VoterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			// get cookies sent from clnt --> server , using req header
			Cookie[] cookies = request.getCookies();
			// null chking
			if (cookies != null) {
				// clnt has sent cookie/s
				for (Cookie c : cookies)
					if (c.getName().equals("voter_details"))
						pw.print("<h5>Voter Details from Cookie " + c.getValue() + "</h5>");

			} else
				pw.print("<h5>No Cookies , Session Tracking failed!!!!!!!</h5>");
			// send logout link to the clnt
			
		}
	}

}
