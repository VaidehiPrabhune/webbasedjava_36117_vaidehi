package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.VoterDaoIMplementation;
import pojo.Voter;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet(urlPatterns ="/login",loadOnStartup = 1)
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VoterDaoIMplementation dao;
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		try {
			dao=new VoterDaoIMplementation();			
		}catch(Exception e)
		{
			// Centralized err handling in servlets
			// Inform WC that init has failed : so that WC won't continue with remaining
			// life cycle of the servlet
			// HOW : throw servlet exc to WC
			throw new ServletException("err in init of " + getClass().getName(),e);
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try(PrintWriter pw=response.getWriter())
		{
			String email=request.getParameter("em");
			String password=request.getParameter("pass");
			Voter voter=dao.validateUser(email, password);
			pw.print(voter);
			if(voter == null) //invalid login
				pw.print("<h5>Invalid Login, Please <a href='login.html'>Retry</a></h5>");			
			else //valid login
			{
				Cookie c1=new Cookie("voter_details",voter.toString());
				response.addCookie(c1);
				if(voter.getRole().equals("admin"))
				{
					response.sendRedirect("adminStatus");
				}
				
				if(voter.getRole().equals("voter"))
				{				
					if((voter.getStatus()== true) )	
						response.sendRedirect("voterStatus");
					else
						response.sendRedirect("CandidateList");
				}
			}
		
		
		
		}catch (Exception e) {
			// inform WC
			throw new ServletException("err in do-post of " + getClass().getName(), e);
		}
	}
	
	public void destroy() {
		try {
			dao.cleanUp();
		}catch(Exception e)
		{
			throw new RuntimeException("in destroy of " + getClass().getName() + " err ", e);
		}
	}

}
