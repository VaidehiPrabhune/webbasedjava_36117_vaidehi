package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CandidateDaoImplementation;
import dao.VoterDaoIMplementation;
import pojo.Candidate;

/**
 * Servlet implementation class CandidateServlet
 */
@WebServlet("/CandidateList")
public class CandidateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CandidateDaoImplementation dao;
	public void init() throws ServletException {
		try {
			dao=new CandidateDaoImplementation();			
		}catch(Exception e)
		{
			// Centralized err handling in servlets
			// Inform WC that init has failed : so that WC won't continue with remaining
			// life cycle of the servlet
			// HOW : throw servlet exc to WC
			throw new ServletException("err in init of " + getClass().getName(),e);
		}
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// set cont type
		response.setContentType("text/html");
		try(PrintWriter pw = response.getWriter()) {
			List<Candidate> candidates=dao.listCandidates();
			pw.println("<h5>Candidates List  :</h5>");
			for (Candidate candidate : candidates) {
				pw.print("<h5>"+candidate+"</h5>");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void destroy() {
		try {
			dao.cleanUp();
		}catch(Exception e)
		{
			throw new RuntimeException("in destroy of " + getClass().getName() + " err ", e);
		}
	}

}
