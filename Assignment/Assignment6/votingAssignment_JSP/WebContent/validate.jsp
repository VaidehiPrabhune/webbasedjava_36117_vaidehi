<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%--IMport JSTL supplied core tag library --%>
 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>

<%--ALL MATCHING setters(req params MATCHING with JB property setters) of bean called by WC--%>
<jsp:setProperty property="*" name="voter"/>
<body>
	<%--invoke B.L method of bean --%>
	<%--Below content will not be sent to client --%>
	<%--<h5>Status :${sessionScope.voter.validateUser()}</h5> --%>
	<%--<h5>Validated user details :${sessionScope.voter.validatedUser}</h5> --%>
	<%--redirecting the client in the NEXT req to next page --%>
	<%--response.sendRedirect + URL rewriting --%>
	<c:redirect url="${sessionScope.voter.validateUser()}.jsp"/>
	
</body>

</html>