<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<jsp:useBean id="voterdata" class="beans.VoterBean" scope="session" />
<body>
	<%--display list of candidates by party and top2candidates analysis:by calling B.L method of candidate bean E.L Syntax --%>
	<h5 align="center">Party wise Analysis</h5>
	<table style="background-color: cyan; margin: auto;" border="1">
		<c:forEach var="c" items="${sessionScope.candidate.partyWiseListCandidates()}">
			<tr>
				<td>${c.key}</td>
				<td>${c.value}</td>
			</tr>
		</c:forEach>
	</table>
	
	<h5 align="center">Top 2 candidates</h5>
	<table style="background-color: cyan; margin: auto;" border="1">
		<c:forEach var="c" items="${sessionScope.candidate.Top2CandidatesList()}">
			<tr>
				<td>${c.candidateId}</td>
				<td>${c.name}</td>			
				<td>${c.party}</td>
				<td>${c.votes}</td>
			</tr>
		</c:forEach>
	</table>
	
	<h4><a href="registerVoter.jsp">Voter Registration</a></h4>
</body>
</html>