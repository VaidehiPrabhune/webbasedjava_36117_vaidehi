<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%--import JSTL supplied core tag library --%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%--check voting status --%>
	<%-- if(!session.getAttribute("voter").getValidatedUser().isStatus())--%>
	<c:choose>
		<c:when test="${!sessionScope.voter.validatedUser.status}">
			<%--increment votes n update voting status --%>
			<%--invoke setter for cid --%>
			<jsp:setProperty property="*" name="candidate" />
			<%--invoke B.L method of candidate bean --%>
			<h5>Voting message :${sessionScope.candidate.updateVotes()}</h5>
			<%--invoke B.L method of voter bean --%>
			<h5>Status :${sessionScope.voter.updateStatus()}</h5>

		</c:when>
		<c:otherwise>
			<h5>You have already voted...</h5>
		</c:otherwise>
	</c:choose>
	<%--invoke bean's method to close DB resources --%>
	${sessionScope.voter.daoCleanUp()}
	${sessionScope.candidate.daoCleanUp()}
	<%--invalidate HttpSession --%>
	<%--pageContext().getSession.invalidate() --%>
	${pageContext.session.invalidate()}
</body>
</html>