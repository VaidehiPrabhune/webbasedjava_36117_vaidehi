package beans;

import java.sql.SQLException;

import dao.VoterDaoImpl;
import pojos.Voter;

public class VoterBean {
 //properties of Java Beans(JB) :private,non static,non transit
	//client conversational state(request parameters)
	private String email,password,name;
	//DAO layer reference :dependency of JB
	private VoterDaoImpl voterDao;
	//store validated user details :VOter POJO
	private Voter validatedUser;
	//default constructor
	public VoterBean() throws Exception
	{
		System.out.println("voter bean constructor");
		//instantiate DAO
		voterDao=new VoterDaoImpl();
	}
	//all setters and getters for properties
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public VoterDaoImpl getVoterDao() {
		return voterDao;
	}
	public void setVoterDao(VoterDaoImpl voterDao) {
		this.voterDao = voterDao;
	}
	public Voter getValidatedUser() {
		return validatedUser;
	}
	public void setValidatedUser(Voter validatedUser) {
		this.validatedUser = validatedUser;
	}
	//add Business logic method for user validation to return navigational output
	public String validateUser() throws SQLException
	{
		System.out.println("in JB : validateUser "+email+" "+password);
		//inoke dao's method for validation
		validatedUser=voterDao.authenticateVoter(email, password);
		if(validatedUser == null)
		{
			//invalid user
			return "login";
		}
		//valid user : check role
		if(validatedUser.getRole().equals("admin"))
			return "admin_status";
		//voter :user :check voting status
		if(validatedUser.isStatus())
			return "voter_status";
		//voter : not yet voted
		return "candidate_list";
		
	}
	//add B.L method to update voting status
	public String updateStatus() throws SQLException
	{
		System.out.println("in B.L update status");
		return voterDao.updateVotingStatus(validatedUser.getVoterId());
	}
	//add B.L method to clean up voter dao
	public void daoCleanUp() throws SQLException
	{
		voterDao.cleanUp();
	}
	//add B.L method to insert voter details
	public String insertVoterDate() throws Exception
	{
		System.out.println("in B.L insert voter");
		boolean status=false;
		String role="voter";
		int insertionStatus= voterDao.insert(name, email, password, status, role);
		if(insertionStatus != 0)
			return "Voter registration successfull";
		return "voter registration failed!!";
	}
}
