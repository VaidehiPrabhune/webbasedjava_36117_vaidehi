package tester;
import dao.VendorDaoImpl;
import org.hibernate.*;
import static utils.HibernateUtils.getSf;

import java.time.LocalDate;
import java.util.Scanner;

public class ListVendors {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try(SessionFactory sf=getSf(); Scanner sc=new Scanner(System.in))
		{
			VendorDaoImpl dao=new VendorDaoImpl();
			System.out.println("Enter date(yr-mon-day) n amount");
			dao.listSpecificVendors(LocalDate.parse(sc.next()), sc.nextDouble()).forEach(System.out::println);			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
