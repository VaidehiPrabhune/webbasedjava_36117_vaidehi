package tester;

import org.hibernate.SessionFactory;

import dao.VendorDaoImpl;
import pojo.Role;
import pojo.Vendor;

import static utils.HibernateUtils.getSf;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.*;
public class RegisterVendor {

	public static void main(String[] args) {
		try(SessionFactory sf=getSf(); Scanner sc=new Scanner(System.in))
		{
			VendorDaoImpl dao=new VendorDaoImpl();
			System.out.println("Enter vendor details name,email,password,regAmount,regDate,role");
			Vendor v=new Vendor(sc.next(), sc.next(), sc.next(),sc.nextDouble(),LocalDate.parse(sc.next()),Role.valueOf(sc.next().toUpperCase()));
			System.out.println(dao.registerVendor(v));
			
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

}
