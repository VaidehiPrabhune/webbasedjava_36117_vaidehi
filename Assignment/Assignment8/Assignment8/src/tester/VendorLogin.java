package tester;
import dao.VendorDaoImpl;
import org.hibernate.*;
import static utils.HibernateUtils.getSf;

import java.time.LocalDate;
import java.util.Scanner;

public class VendorLogin {

	public static void main(String[] args) {
		try(SessionFactory sf=getSf();Scanner sc=new Scanner(System.in))
		{
			VendorDaoImpl dao=new VendorDaoImpl();
			System.out.println("Enter email and password");
			System.out.println(dao.VendorLogin(sc.next(), sc.next()));
		}

	}

}
