package pojo;

//id(Integer),name,email(unique),password,reg amount,reg date (LocalDate),role (enum --vendor / admin)
import java.time.LocalDate;
import javax.persistence.*;

@Entity
@Table(name = "vendors_tbl")
public class Vendor {
	@Id // PK
	@GeneratedValue(strategy = GenerationType.IDENTITY) // strategy = AUTO will be replaced : auto_increment
	@Column(name = "vendor_id")
	private Integer id;
	@Column(length = 30)
	private String name;
	@Column(length = 30, unique = true)
	private String email;
	@Column(length = 30)
	private String password;
	@Column(name = "reg_amount")
	private double regAmount;
	@Column(name = "reg_date")
	private LocalDate regDate;
	@Enumerated(EnumType.STRING)
	@Column(name = "user_role", length = 20)
	private Role role;

	public Vendor() {
		System.out.println("in vendor ctor");
	}

	public Vendor(String name, String email, String password, double regAmount, LocalDate regDate, Role role) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.regAmount = regAmount;
		this.regDate = regDate;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getRegAmount() {
		return regAmount;
	}

	public void setRegAmount(double regAmount) {
		this.regAmount = regAmount;
	}

	public LocalDate getRegDate() {
		return regDate;
	}

	public void setRegDate(LocalDate regDate) {
		this.regDate = regDate;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Vendor [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + ", regAmount="
				+ regAmount + ", regDate=" + regDate + ", role=" + role + "]";
	}

}
