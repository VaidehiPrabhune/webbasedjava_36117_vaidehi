package dao;

import org.hibernate.*;
import static utils.HibernateUtils.getSf;

import java.time.LocalDate;
import java.util.List;

import pojo.Role;
import pojo.Vendor;

public class VendorDaoImpl implements IVendorDao{

	@Override
	public String registerVendor(Vendor v) {
		//v : transient
		// session from SF : getCurntSession
		String mesg="Vendor reg failed";
		Session session=getSf().getCurrentSession();
		//begin tx
		Transaction tx=session.beginTransaction();
		try {
			session.save(v);
			tx.commit();
			mesg="Vendor registered successfully with ID"+v.getId();
		}catch (HibernateException e) {
			if(tx != null)
				tx.rollback();
			throw e;
		}
		// v : detached
		return mesg;
	}

	//List all vendors , registered after specific reg date & reg amount < specified amt.
	@Override
	public List<Vendor> listSpecificVendors(LocalDate regDate, double amount) {
		List<Vendor> vendors=null;
		String jpql="select v from Vendor v where v.regDate>:dt and v.regAmount<:amt and v.role=:rl";
		Session session=getSf().getCurrentSession();
		//begin tx
		Transaction tx=session.beginTransaction();
		try {
			//create query obj,set IN params,exexc query
			vendors=session.createQuery(jpql, Vendor.class).setParameter("dt", regDate).setParameter("amt", amount).setParameter("rl", Role.VENDOR).getResultList();
			tx.commit();
		}catch (HibernateException e) {
			if(tx != null)
				tx.rollback();
			throw e;
		}
		return vendors;
	}

	@Override
	public List<Vendor> applyDiscount(double discount, LocalDate date) {
		List<Vendor> vendors=null;
		String jpql="select v from Vendor v where v.regDate<:dt and v.role=:rl";
		Session session=getSf().getCurrentSession();
		//begin tx
		Transaction tx=session.beginTransaction();
		try {
			//get the list of selecting vendors
			vendors=session.createQuery(jpql, Vendor.class).setParameter("dt", date).setParameter("rl", Role.VENDOR).getResultList();
			vendors.forEach(v->v.setRegAmount(v.getRegAmount()-discount));//internal iteration
			tx.commit();
			
		}catch (HibernateException e) {
			if(tx != null)
				tx.rollback();
			throw e;
		}
		return vendors;
	}

	@Override
	public String VendorLogin(String email, String password) {
		Vendor v=null;
		String mesg="Vendor login failed";
		String jpql="select v from Vendor v where v.email=:em and v.password=:pass";
		Session session=getSf().getCurrentSession();
		Transaction tx=session.beginTransaction();
		try {
			v=session.createQuery(jpql, Vendor.class).setParameter("em", email).setParameter("pass",password).getSingleResult();
			tx.commit();
			mesg="Login successfull!!";
		}catch (HibernateException e) {
			if(tx != null)
				tx.rollback();
			throw e;
		}
		return mesg;
	}

}
