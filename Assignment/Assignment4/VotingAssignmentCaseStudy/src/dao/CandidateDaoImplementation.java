package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import static Utils.DBUtils.getConnection;
import pojo.Candidate;
import pojo.Voter;

public class CandidateDaoImplementation implements ICandidateDao {
	// data members:instance variables
	private Connection connection = null;
	PreparedStatement stmtInsert;
	PreparedStatement stmtUpdate;
	PreparedStatement stmtDelete;
	PreparedStatement stmtSelect;
	PreparedStatement stmtSelectCand;
	PreparedStatement stmtSelectCandTop2;

	public CandidateDaoImplementation() throws Exception {
		this.connection = getConnection();
		this.stmtInsert = this.connection.prepareStatement("INSERT INTO candidates VALUES(?,?,?,?)");
		this.stmtUpdate = this.connection.prepareStatement("UPDATE candidates SET votes=? WHERE id=?");
		this.stmtDelete = this.connection.prepareStatement("DELETE FROM candidates WHERE id=?");
		this.stmtSelect = this.connection.prepareStatement("SELECT * FROM candidates");
		this.stmtSelectCand = this.connection.prepareStatement("SELECT * FROM candidates WHERE id=?");
		this.stmtSelectCandTop2=this.connection.prepareStatement("SELECT * FROM candidates order by votes desc limit 2");
		System.out.println("candidate dao created.....");
	}

	public int insert(Candidate candidate) throws Exception {
		this.stmtInsert.setInt(1, candidate.getId());
		this.stmtInsert.setString(2, candidate.getName());
		this.stmtInsert.setString(3, candidate.getParty());
		this.stmtInsert.setInt(4, candidate.getVotes());
		return this.stmtInsert.executeUpdate();
	}


	public int delete(int candidateId) throws Exception {
		this.stmtDelete.setInt(1, candidateId);
		return this.stmtDelete.executeUpdate();
	}

	@Override
	public List<Candidate> listCandidates() throws Exception {
		List<Candidate> candidates=new ArrayList<Candidate>();
		try( ResultSet rs = this.stmtSelect.executeQuery() ){
			while( rs.next())
			{
				Candidate candidate	=new Candidate(rs.getInt("id"),rs.getString("name"),rs.getString("party"),rs.getInt("votes")) ;
				candidates.add(candidate);
			}
		}
		return candidates;
	}

	@Override
	public String incrementVotes(int candidateId) throws Exception {
		stmtSelectCand.setInt(1, candidateId);
		Candidate c=null;
		try( ResultSet rst=stmtSelectCand.executeQuery()){
			if(rst.next())
			c=new Candidate(candidateId,rst.getString(2),rst.getString(3),rst.getInt(4));	
		}
		int count=c.getVotes()+1;
		this.stmtUpdate.setInt(1, count);
		this.stmtUpdate.setInt(2, candidateId);
		this.stmtUpdate.executeUpdate();
		 return null;
	}
	//Cleaning up of DB resources
	public void cleanUp() throws Exception{
		if(connection != null)
			connection.close();
	}

	public List<Candidate> top2Analysis() throws Exception{
		List<Candidate> candidates=new ArrayList<Candidate>();
		try( ResultSet rs = this.stmtSelectCandTop2.executeQuery() ){
			while( rs.next())
			{
				Candidate candidate	=new Candidate(rs.getInt("id"),rs.getString("name"),rs.getString("party"),rs.getInt("votes")) ;
				candidates.add(candidate);
			}
		}
		return candidates;
	}

	public LinkedHashMap<String, Integer> partyWiseAnalysis() throws Exception{
	LinkedHashMap<String, Integer> partyWiseVotes=new LinkedHashMap<String, Integer>();
	try( ResultSet rs = this.stmtSelect.executeQuery() ){
		while( rs.next())
		{
			Candidate candidate	=new Candidate(rs.getInt("id"),rs.getString("name"),rs.getString("party"),rs.getInt("votes")) ;
			partyWiseVotes.put(candidate.getParty(),candidate.getVotes());
		}
	}
	return partyWiseVotes;
	}

	
}
