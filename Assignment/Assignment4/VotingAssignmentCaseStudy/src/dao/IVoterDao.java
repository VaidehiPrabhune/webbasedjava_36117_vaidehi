package dao;

import pojo.Voter;

public interface IVoterDao {
	public Voter validateUser(String email,String password) throws Exception;
	String updateVotingStatus(int voterId)  throws Exception;
}
