package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImplementation;
import dao.VoterDaoIMplementation;
import pojo.Voter;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet(urlPatterns ="/login",loadOnStartup = 1)
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VoterDaoIMplementation voterdao;
	private CandidateDaoImplementation candidatedao;
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		try {
			voterdao=new VoterDaoIMplementation();	
			candidatedao=new CandidateDaoImplementation();
		}catch(Exception e)
		{
			// Centralized err handling in servlets
			// Inform WC that init has failed : so that WC won't continue with remaining
			// life cycle of the servlet
			// HOW : throw servlet exc to WC
			throw new ServletException("err in init of " + getClass().getName(),e);
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try(PrintWriter pw=response.getWriter())
		{
			String email=request.getParameter("em");
			String password=request.getParameter("pass");
			Voter voter=voterdao.validateUser(email, password);
			pw.print(voter);
			if(voter == null) //invalid login
				pw.print("<h5>Invalid Login, Please <a href='login.html'>Retry</a></h5>");			
			else //valid login
			{
				//get HttpSession from WC
				HttpSession session=request.getSession();
				//confirm if session is old or not
				System.out.println("From login page HS(HttpSession)"+session.isNew());
				//session id
				System.out.println("Session Id:"+session.getId());
				//save validated candidate details under the session scope
				session.setAttribute("voter_details", voter);
				session.setAttribute("voter_dao", voterdao);
				session.setAttribute("candidate_dao", candidatedao);
				
				if(voter.getRole().equals("admin"))
				{
					response.sendRedirect("adminStatus");
				}
				
				if(voter.getRole().equals("voter"))
				{				
					if((voter.getStatus()== true) )	
						response.sendRedirect("voterStatus");
					else
						response.sendRedirect("CandidateList");
				}
				
				// invalidate HttpSession
				//session.invalidate();
			}
			
		
		
		}catch (Exception e) {
			// inform WC
			throw new ServletException("err in do-post of " + getClass().getName(), e);
		}
	}
	
	public void destroy() {
		try {
			voterdao.cleanUp();
			candidatedao.cleanUp();
		}catch(Exception e)
		{
			throw new RuntimeException("in destroy of " + getClass().getName() + " err ", e);
		}
	}

}
