package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImplementation;
import pojo.Candidate;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/adminStatus")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CandidateDaoImplementation candidatedao;
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			pw.print("<h5>Admin Login Successful </h5>");
			pw.print("Top 2 Candidates Listed Below ");
			//get HttpSession from WC
			HttpSession session =request.getSession();
			candidatedao=(CandidateDaoImplementation) session.getAttribute("candidate_dao");
			List<Candidate> candidatesTop2=candidatedao.top2Analysis();
			for (Candidate candidate : candidatesTop2) {
				pw.print("<h5>"+"Id :"+candidate.getId()+" name :"+candidate.getName()+" Party:"+candidate.getParty()+" Total Votes :"+candidate.getVotes()+"</h5>");
			}
			
			LinkedHashMap<String,Integer> partyWiseVotesList=candidatedao.partyWiseAnalysis();
			partyWiseVotesList.forEach((key,value) ->{
				pw.print("Party :"+key+"   votes :"+value);
				pw.print("</br>");
			});
			pw.print("<a href='login.html'>Logout here</a>");
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
