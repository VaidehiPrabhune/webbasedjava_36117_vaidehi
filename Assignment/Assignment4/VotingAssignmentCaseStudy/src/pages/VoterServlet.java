package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pojo.Voter;

/**
 * Servlet implementation class VoterServlet
 */
@WebServlet("/voterStatus")
public class VoterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			//get HttpSession from WC
			HttpSession session=request.getSession();
			//get candidate dao from HS
			Voter voter=(Voter) session.getAttribute("voter_details");
			pw.print("Hello "+voter.getName());
			pw.print("You have already voted!! Thanks for voting!!" +"</br>");
			pw.print(voter.toString());
			
			pw.print("<a href='login.html'>Logout here</a>");				
		}
	}

}
