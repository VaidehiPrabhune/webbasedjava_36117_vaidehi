package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImplementation;
import dao.VoterDaoIMplementation;
import pojo.Voter;

/**
 * Servlet implementation class VoterDetailsServlet
 */
@WebServlet("/add_your_vote")
public class VoterDetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unused")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set content type
		response.setContentType("text/html");
		//open pw
		try(PrintWriter pw=response.getWriter())
		{
			
			//get HttpSession from WC
			HttpSession session=request.getSession();
			//get candidate dao from HS
			CandidateDaoImplementation candidateDao=(CandidateDaoImplementation) session.getAttribute("candidate_dao");
			VoterDaoIMplementation voterDao=(VoterDaoIMplementation) session.getAttribute("voter_dao");
			Voter voter=(Voter) session.getAttribute("voter_details");
			
			if(candidateDao != null)
			{
				String candidateId=request.getParameter("candidate");
				int candidateID=Integer.parseInt(candidateId);
				candidateDao.incrementVotes(candidateID);
				voterDao.updateVotingStatus(voter.getId());
				pw.print("Thanks for voting !!!!!"+"</br>");
				pw.print("<a href='login.html'>Logout here</a>");
								
			}else
				pw.print("<h5>No Cookies, Session Tracking Failed!!!!</h5>");
			
		}catch(Exception e)
		{
			//re throw exc to the WC
			throw new ServletException("err in do-get of"+getClass().getName(),e);
		}
	}

}
