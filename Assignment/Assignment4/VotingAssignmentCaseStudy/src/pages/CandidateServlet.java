package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImplementation;
import dao.VoterDaoIMplementation;
import pojo.Candidate;

/**
 * Servlet implementation class CandidateServlet
 */
@WebServlet("/CandidateList")
public class CandidateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CandidateDaoImplementation candidatedao;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// set cont type
		response.setContentType("text/html");
		try(PrintWriter pw = response.getWriter()) {
			//get HttpSession from WC
			HttpSession session =request.getSession();
			//get candidatedao from HC
			candidatedao=(CandidateDaoImplementation) session.getAttribute("candidate_dao");
			List<Candidate> candidates=candidatedao.listCandidates();
			pw.println("<h5>Candidates List  :</h5>");
			
			//dynamic form generation
			pw.print("<form action='add_your_vote'>");
			//dynamic generation of radio buttons
			for (Candidate candidate : candidates) {
				pw.print("<input type='radio' id="+candidate.getId()+" name='candidate' value="+candidate.getId()+">");
				pw.print("<label for="+candidate.getId()+"> Name : "+candidate.getName() + "  Party : " +candidate.getParty() +" </label></br>");
			}
			//add submit button :Add your vote
			pw.print("<input type='submit' value='Add your vote'>");
			pw.print("</form>");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
