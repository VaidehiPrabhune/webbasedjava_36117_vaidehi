package dao;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import pojos.Book;

import static utils.DBUtils.fetchConnection;

public class BookDaoImplementation implements IBookDao{
	private Connection cn;
	private PreparedStatement pst1,pst2,pst3;
	
	//const: will be invoked by servlet
	public BookDaoImplementation() throws Exception {
		//get cn from DBUtils
		cn=fetchConnection();
		//pst1:for getting all categories
		pst1=cn.prepareStatement("select distinct category from dac_books");
		
		//pst2=method to fetch all book from a selected category : called by CategoryDetailsServlet
		pst2=cn.prepareStatement("select * from dac_books where category=?");
		
		//pst3=add a method to fetch actual book details selected by the user :called by ShowCartServlet n Checkout
		pst3=cn.prepareStatement("select * from dac_books where id=?");
		System.out.println("book dao created");
	}
	
	public void cleanUp() throws Exception
	{
		if(pst1 != null)
			pst1.close();

		if(pst2 != null)
			pst2.close();

		if(pst3 != null)
			pst3.close();
		
		if(cn != null)
			cn.close();
		
		System.out.println("book dao cleaned up....");
	}

	@Override
	public List<String> getAllCategories() throws Exception {
		//create empty ArrayList to hold the category names
		List<String> categories=new ArrayList<>();
		try(ResultSet rst=pst1.executeQuery())
		{
			while(rst.next())
				categories.add(rst.getString(1));//1 is no. of col postion appear in result set
		}
		return categories;
	}

	@Override
	public List<Book> getAllBookByCategory(String CategoryName) throws Exception {
		List<Book> availableBooks =new ArrayList<>();
		//set IN param:category name
		pst2.setString(1, CategoryName);
		try(ResultSet rst=pst2.executeQuery())
		{
			while(rst.next())
				availableBooks.add(new Book(rst.getInt(1), rst.getString(2), rst.getString(3), CategoryName, rst.getDouble(5)));
			
		}
		return availableBooks;
	}

	@Override
	public Book getBookDetails(int bookId) throws Exception {
		//Set IN param : book id
		pst3.setInt(1, bookId);
		try(ResultSet rst=pst3.executeQuery())
		{
			if(rst.next())
				return new Book(bookId, rst.getString(2), rst.getString(3), rst.getString(4), rst.getDouble(5));
		}
		return null;
	}
	
}
