package dao;

import pojos.Customer;
import java.sql.*;



//can directly access static method of a class
import static utils.DBUtils.fetchConnection;


public class CustomerDaoImpl implements ICustomerDao {
	//data members : instance vars
	private Connection cn;
	private PreparedStatement pst1,pst2;
	
	//constr : arg less constr
	public CustomerDaoImpl() throws Exception{
		// get cn from db utils
		cn=fetchConnection();
		//create pre parsed n pre compiled stmt to hold validation query
		pst1=cn.prepareStatement("select * from my_customers where email=? and password=?");
		pst2 =cn.prepareStatement("INSERT INTO my_customers (email,password,reg_amt) VALUES (?,?,?)");
		System.out.println("cutomer dao created...");
	}
	

	@Override
	public Customer authenticateCustomer(String email, String pwd) throws Exception {
		// set IN params
		pst1.setString(1, email);
		pst1.setString(2, pwd);
		try(ResultSet rst=pst1.executeQuery())
		{
			if(rst.next())
				return new Customer(rst.getInt(1), email, pwd, rst.getDouble(4), rst.getDate(5));
		}
		return null;
	}
	
	public int insert(String UserName,String password,double amount) throws Exception {
		pst2.setString(1, UserName);
		pst2.setString(2, password);
		pst2.setDouble(3, amount);	
		pst2.execute();
		return pst2.getUpdateCount();
	}
	
	//cleaning  up of DB resources
	public void cleanUp() throws Exception
	{
		if(pst1 != null)
			pst1.close();

		if(pst2 != null)
			pst2.close();
		
		if(cn != null)
			cn.close();
		System.out.println("customer dao cleaned up...");
	}
	

}
