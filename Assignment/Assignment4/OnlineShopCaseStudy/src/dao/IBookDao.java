package dao;

import java.util.List;

import pojos.Book;

public interface IBookDao {
	//add a method to fetch all distinct category names from the database:called by categoryServlet
	List<String> getAllCategories() throws Exception;
	
	//add a method to fetch all book from a selected category : called by CategoryDetailsServlet
	List<Book> getAllBookByCategory(String CategoryName) throws Exception;
	
	//add a method to fetch actual book details selected by the user :called by ShowCartServlet n Checkout
	Book getBookDetails(int bookId) throws Exception;
}
