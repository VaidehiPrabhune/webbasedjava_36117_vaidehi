package utils;

import java.sql.*;

public class DBUtils {
//add static method to return FIXED DB connection
	public static Connection fetchConnection() throws ClassNotFoundException, SQLException {
		// load JDBC Driver
		Class.forName("com.mysql.cj.jdbc.Driver");
		// get cn from DriverMgr
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/webJava_db?useSSL=false",
				"dac", "dac");
	}

}
