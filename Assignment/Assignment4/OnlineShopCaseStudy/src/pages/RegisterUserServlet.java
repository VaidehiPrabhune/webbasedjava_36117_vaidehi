package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookDaoImplementation;
import dao.CustomerDaoImpl;
import pojos.Customer;
import sun.util.resources.CalendarData;

/**
 * Servlet implementation class RegisterUserServlet
 */
@WebServlet("/register")
public class RegisterUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CustomerDaoImpl customerDao;
	public void init() throws ServletException {
		// create dao instance
		try {
			customerDao = new CustomerDaoImpl();
		} catch (Exception e) {
			// Centralized err handling in servlets
			// Inform WC that init has failed : so that WC won't continue with remaining
			// life cycle of the servlet
			// HOW : throw servlet exc to WC
			throw new ServletException("err in init of " + getClass().getName(), e);

		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		//java.sql.Date date=Date.valueOf(LocalDate.now());
		try (PrintWriter pw = response.getWriter()) {	
			//get HttpSession from WC
			//HttpSession session=request.getSession();
			//session.setAttribute("cust_dao", customerDao);
			
			String UserName=request.getParameter("em");
			String password=request.getParameter("pass");
			double amount=500;
			int count = customerDao.insert(UserName,password,amount);
			if(count == 1)
			{
				pw.print("Successfully Registered!!");
				pw.print("<a href='login.html'>Login</a>");
			}
			else
			{
				pw.print("Registration Fail!! Please register again");
				pw.print("<a href='reg_form.html'>Register</a>");
			}
			
		}catch (Exception e) {
			// inform WC
			throw new ServletException("err in do-post of " + getClass().getName(), e);
		}
	}

	public void destroy() {
		try {
			customerDao.cleanUp();
		} catch (Exception e) {
			// System.out.println("in destroy of "+getClass().getName()+" err "+e);
			throw new RuntimeException("in destroy of " + getClass().getName() + " err ", e);
		}
	}
}
