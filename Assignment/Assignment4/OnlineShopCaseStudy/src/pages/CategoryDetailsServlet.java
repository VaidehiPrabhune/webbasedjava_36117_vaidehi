package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookDaoImplementation;
import pojos.Book;

/**
 * Servlet implementation class CategoryDetailsServlet
 */
@WebServlet("/category_details")
public class CategoryDetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set content type
		response.setContentType("text/html");
		//open pw
		try(PrintWriter pw=response.getWriter())
		{
			//get HttpSession from WC
			HttpSession session=request.getSession();
			//get book dao from HS
			BookDaoImplementation bookDao=(BookDaoImplementation)session.getAttribute("book_dao");
			if (bookDao != null) {
				//get chosen category : from request param (clnt)
				//http://localhost:8080/day3.1/category_details?cat_name=angular
				String chosenCategory=request.getParameter("cat_name");
				//invoke book dao's method to fetch all available books from the chosen category
				List<Book> bookList = bookDao.getAllBookByCategory(chosenCategory);
				//display chosen category
				pw.print("<h5> Books Under "+chosenCategory+" category</h5>");
				//dynamic form generation
				pw.print("<form action='add_to_cart'>");
				//dyn gen checkboxes
				for(Book b : bookList)
					pw.print("<input type='checkbox' name='book_id' value="+b.getBookId()+">"+b+"<br>");
				
				//add submit button :Add To Cart
				pw.print("<input type='submit' value='Add To Cart'>");
				pw.print("</form>");
			} else
				pw.print("<h5>No Cookies, Session Tracking Failed!!!!</h5>");
			
			
		}catch(Exception e)
		{
			//re throw exc to the WC
			throw new ServletException("err in do-get of"+getClass().getName(),e);
		}
	}

}
