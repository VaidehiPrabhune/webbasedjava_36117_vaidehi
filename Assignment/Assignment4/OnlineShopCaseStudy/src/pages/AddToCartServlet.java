package pages;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AddToCartServlet
 */
@WebServlet("/add_to_cart")
public class AddToCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//http://localhost:8080/day3.1/add_to_cart?book_id=5&book_id=6
		//get selected book ids from request para:sent by client
		String[] bookIds=request.getParameterValues("book_id");
		//get HttpSession from WC
		HttpSession session=request.getSession();
		//get cart from session scope
		List<Integer> cart=(List<Integer>)session.getAttribute("shopping_cart");
		//populate the cart with selected book ids
		for(String s:bookIds)
			cart.add(Integer.parseInt(s)); //int --> Integer(auto boxing)
		System.out.println("cart contents "+cart);
		//redirect the client to category page in the NEXT request
		response.sendRedirect("category");
		
	}

}
