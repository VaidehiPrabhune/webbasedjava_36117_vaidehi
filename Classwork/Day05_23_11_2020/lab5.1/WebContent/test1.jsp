<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<h5>From Test1 page....</h5>
	<%--name=abc&age=25 --%>
	<%--Display User's name n age using EL syntax --%>
	<h5>Hello , ${param.name}</h5> <%--blank --%>
	<h5>Age : ${param.age}</h5>    <%--blank --%>
	<%--Add these user details under session scope --%>
	<%
	out.flush();
		//adding session scope attr
	session.setAttribute("attr2", 2345);
	//create page , req , appln scoped attrs
	//how to add attribute under current page? :using PageContext:setAttribute()
	pageContext.setAttribute("attr1", 1234);
	request.setAttribute("user_details", request.getParameter("name") + ":" + request.getParameter("age"));	
	application.setAttribute("attr3", 4567);
	//clnt pull II : send redirect
	//response.sendRedirect("test2.jsp");
	
	//server pull: forword Scenario
	//1.Create request dispatcher
	RequestDispatcher rd=request.getRequestDispatcher("test2.jsp");
	//forword
	//rd.forward(request, response);
	//include
	rd.include(request, response);//WC :does'nt discard JSP writers buffer,will call test2_jsp class =>_jspService() method
	%>
	<h2>contents after post include</h2>
	
</body>
</html>