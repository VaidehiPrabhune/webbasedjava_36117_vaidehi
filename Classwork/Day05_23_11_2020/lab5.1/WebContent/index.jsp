<%@page import="java.time.LocalDateTime"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Welcome Page</title>
</head>
<body>
	<h3>
	Welcome 2 jsp
	<%=LocalDateTime.now() %></h3>
	<h4>Session :<%=session.getId() %></h4>
	<h4>Session Timeout<%=session.getMaxInactiveInterval() %></h4>
	<h5>
	<a href="comments.jsp">Test Comments</a>
	</h5>
	<h5>
	<a href="login.jsp">Test Scriplets</a>
	</h5>
	<h5>
	<a href="test1.jsp?name=abc&age=25">Testing EL syntax n Attributes</a>
	</h5>
	<h5>
	<a href="test3.jsp">Testing JSP Declarations</a>
	</h5>
	<%-- <h4>Page context:<%=pageContext %></h4>
	<h4>page :<%=page %></h4> --%>
	
</body>
</html>