<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%--pageContext.getSession().getId() --%>
<h5>JSession ID: ${pageContext.session.id}</h5>
<%--cookie.get("JSESSIONID").getValue() --%>
<h5>JSessionID via cookie ${cookie.JSESSIONID.value}</h5>
<%--Display attributes from diff scopes :using EL syntax --%>
<h5>Page Scoped Attr:${pageScope.attr1}</h5>  <%--blank --%>
<h5>Request Scoped Attr:${requestScope.user_details}</h5>  <%--NOnblank --%>
<h5>Session Scoped Attr:${sessionScope.attr2}</h5>  <%--Nonblank --%>
<h5>Application Scoped Attr:${applicationScope.attr3}</h5>  <%--Nonblank --%>
<h5>Request Param:${param.name}</h5>  <%--Nonblank same req is over pages--%>
<h5>Param map :${param}</h5> <%--Nonblank --%>

</body>
</html>