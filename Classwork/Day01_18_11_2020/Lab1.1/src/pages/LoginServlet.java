package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(description = "Authentication Servlet", urlPatterns = { "/authenticate" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set resp content type
		response.setContentType("text/html");
		//open printwriter to send response from servlet ->client
		try(PrintWriter pw=response.getWriter())
		{
			pw.print("<h5>Email :"+request.getParameter("em")+"</h5>");
			pw.print("<h5>Password :"+request.getParameter("pass")+"</h5>");
		}
		
	}

}
