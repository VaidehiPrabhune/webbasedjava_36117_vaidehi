package pages;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

//URL:http://host:port/day1.1/test
//URL : ctx path (day1.1)
//URL pattern :/test

@WebServlet(value= {"/test","/test2"},loadOnStartup=1) 

//Web cotainer processes this annotation at deployment time & adds the mapping between incoming url pattern and servlet
//WC creates an empty Map(HashMap) 
//key:URL pattern(/test)
//value: FQ servlet class name
public class HelloServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("in doGet"+Thread.currentThread());
		//set response content type for client browser
		resp.setContentType("text/html");
		//open print writer to send response from servlet ->client(text type)
		try(PrintWriter pw=resp.getWriter())
		{
			pw.print("<h5>Hello from servlet @ "+LocalDateTime.now()+"</h5>");//for dynamic content we used now
		}
	}

	@Override
	public void destroy() {
		System.out.println("in destroy"+Thread.currentThread());
	}

	@Override
	public void init() throws ServletException {
		System.out.println("in init"+getClass().getName()+Thread.currentThread());
	}
	
}
