package dao;

import pojos.Role;
import pojos.User;
import static utils.HibernateUtils.getSf;

import java.util.Date;
import java.util.List;

import org.hibernate.*;
//Below is native hibernate based DAO layer.(completely hibernate specific)

public class UserDaoImpl implements IUserDao {

	@Override
	public String registerUser(User user) {
		String mesg = "User reg failed!!";
		// user:TRANSIENT(not in L1 cache n not in DB):only in java heap
		// 1.get session from SF :getCurrentSession
		Session session = getSf().getCurrentSession();
		Session session2 = getSf().getCurrentSession();
		System.out.println(session == session2); // true
		// begin tx
		Transaction tx = session.beginTransaction();// db connection is pooled out and wrapped in session and return to
													// caller
		System.out.println("after begin tx : session open " + session.isOpen() + " conn " + session.isConnected()); // true
																													// true
		// EMPTY l1 cache is created

		try {
			// insert new user's info
			Integer id = (Integer) session.save(user);// user :PERSISTENT(only added into L1 cache but not part of DB)
			System.out.println("generated id" + id);
			tx.commit();// Hibernate performs :auto dirty checking:insert query :to syn state of
						// L1 cache with that DB ,session implicitly closed=>DB conn return to pool and
						// L1 cache destroyed
			mesg = "User regitered with ID" + id;
			System.out.println("after commit tx : session open " + session.isOpen() + " conn " + session.isConnected()); // false
																															// false

		} catch (HibernateException e) {
			// rollback tx n throw the exc to the caller
			tx.rollback();// session implicitly closed=>DB conn return to pool and L1 cache destroyed
			throw e;
		}
		System.out.println("before returninig: session open " + session.isOpen() + " conn " + session.isConnected()); // false
																														// false
		return mesg;// user : DETACHED state(from L1 cache gone to DB i.e now its part of DB)
	}

	@Override
	public User fetchUserDetails(int userId) {
		User u = null;// u : Not applicable no state
		// session
		Session session = getSf().getCurrentSession();// new session,empty cache
		// tx
		Transaction tx = session.beginTransaction();
		try {
			// Session API:T get(Class<T>cls,Serializable id)
			u = session.get(User.class, userId);// int-->Integer(auto boxing)-->Serializable(up casting)
			// u :in case of valid id,u :PERSISTENT(part of L1 cache)
			u = session.get(User.class, userId);// from cache
			u = session.get(User.class, userId);// from cache

			tx.commit();// auto dirty checking :no queries,db conn returns to pool L1 cache destroyed
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db conn returns to pool L1 cache destroyed
			throw e;
		}
		return u; // u:DETACHED
	}

	@Override
	public List<User> fetchAllUserDetails() {
		List<User> users = null;// users :null (no state applicable)
		String jpql = "select u from User u";
		// get session from sessionFactory
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			// create query from session, execute the same
			users = session.createQuery(jpql, User.class).getResultList();
			// users=session.createQuery(jpql, User.class).getResultList();
			// users=session.createQuery(jpql, User.class).getResultList();
			// users:list of PERSISTENT pojos
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db conn returns to pool L1 cache destroyed
			throw e;
		}
		return users;//// users:list of DETACHED pojos
	}

	// Display all users registered between strt date n end date & under a specific
	// role
	@Override
	public List<User> fetchSelectedUserDetails(Date strtDate, Date endDate, Role userRole) {
		List<User> users = null;
		String jpql = "select u from User u where u.regDate between :start and :end and u.role=:rl";
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, User.class).setParameter("start", strtDate).setParameter("end", endDate)
					.setParameter("rl", userRole).getResultList();
			// users : list of persistent pojos/entities
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db conn returns to pool L1 cache destroyed
			throw e;
		}
		return users;

	}

	@Override
	public List<String> fetchSelectedUserNames(Date strtDate, Date endDate, Role userRole) {

		List<String> users = null;
		String jpql = "select u.name from User u where u.regDate between :start and :end and u.role=:rl";
		// get session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, String.class).setParameter("start", strtDate).setParameter("end", endDate)
					.setParameter("rl", userRole).getResultList();
			// users : list of persistent pojos/entities
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		return users;

	}

	// user names,reg amount,reg date
	@Override
	public List<User> fetchSelectedDetails(Date strtDate, Date endDate, Role userRole) {
		List<User> users = null;
		String jpql = "select new pojos.User(name,regAmount,regDate) from User u where u.regDate between :start and :end and u.role=:rl";
		// get session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, User.class).setParameter("start", strtDate).setParameter("end", endDate)
					.setParameter("rl", userRole).getResultList();
			// users : list of persistent pojos/entities
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		return users;
	}

	//1.change password
	//i/p -> user email,old password,new password
	@Override
	public String changePassword(String email, String oldPwd, String newPwd) 	{
		String mesg="Chaining password failed!!!";
		String jpql="select u from User u where u.email=:em and u.password=:pass";//select jpql : for authentication
		//get hibernate session from SF
		Session session=getSf().getCurrentSession();
		//begin transaction
		Transaction tx=session.beginTransaction();
		User u=null;
		try {
			//create query,set IN params,get single result
			 u=session.createQuery(jpql, User.class).setParameter("em",email).setParameter("pass",oldPwd).getSingleResult();
			//in case of valid credentials : returns : PERSISTENT pojo reference
			//no null checking required since this method throws exception in case of no result found.
			//u:PERSISTENT:exist in L1 cache,exist in DB
			u.setPassword(newPwd);//abcd :modifying state of PERSISTENT pojo
			tx.commit();// auto dirty chking : no queries , db cn rets to the pool n L1 cache is destroyed
			mesg="Updated password....";
		}catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		//u.DETACHED :hibernate does not propogate the change done to state of detached pojo --> DB
		u.setPassword(newPwd.toUpperCase());//ABCD
		return mesg;
	}

}
