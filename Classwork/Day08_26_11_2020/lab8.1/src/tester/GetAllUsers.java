package tester;
import static utils.HibernateUtils.getSf;
import org.hibernate.*;

import dao.UserDaoImpl;

public class GetAllUsers {

	public static void main(String[] args) {
		// Testing bootstrapping of hibernate configuration (creating singleton n
		// immutable instance of SessionFactory (SF)
		try(SessionFactory sf=getSf())
		{
			System.out.println("Hibernate up n running....");
			//create dao instance
			UserDaoImpl dao=new UserDaoImpl();
			System.out.println("Existing Users :");
			dao.fetchAllUserDetails().forEach(System.out::println);
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

}
