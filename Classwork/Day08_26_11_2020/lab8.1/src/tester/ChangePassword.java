package tester;
import static utils.HibernateUtils.getSf;

import java.util.Scanner;

import org.hibernate.*;

import dao.UserDaoImpl;

public class ChangePassword {

	public static void main(String[] args) {
		// Testing bootstrapping of hibernate configuration (creating singleton n
		// immutable instance of SessionFactory (SF)
		try(SessionFactory sf=getSf();Scanner sc=new Scanner(System.in))
		{
			//dao insatnce
			UserDaoImpl dao=new UserDaoImpl();
			System.out.println("Enter User email,old password ,new password");
			System.out.println(dao.changePassword(sc.next(), sc.next(), sc.next()));
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

}
