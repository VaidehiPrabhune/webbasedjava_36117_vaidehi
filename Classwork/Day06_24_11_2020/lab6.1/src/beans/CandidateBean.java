package beans;

import java.sql.SQLException;
import java.util.List;

import dao.CandidateDaoImpl;
import pojos.Candidate;

public class CandidateBean {
    //properties
	private CandidateDaoImpl candidateDao;
	//clients request parameter :chosen candidate id
	//clnt's request parameter : chosen candidate id : int (WC :setter : parsing)
	private int cid;
	
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	//constructor :arg less constr
	public CandidateBean() throws Exception {
		System.out.println("in candidate bean");
		//create dao instance
		candidateDao=new CandidateDaoImpl();
	}
	//no getters n setters required for dao since its a internal property made only for bean
	//B.L : to fetch list of all candidates
	public List<Candidate> getCandidates() throws SQLException
	{
		System.out.println("in B.L candidate bean"); //BL=>business logic
		return candidateDao.getAllCandidates();
	}
	//B.L method to increment selected candidate votes 
	public String updateVotes() throws SQLException
	{
			System.out.println("in B.L cid="+cid);
			//invoke candidate dao's method 
			return candidateDao.incrementVotes(cid);
	}
	//add B.L method to clean up voter dao
	public void daoCleanUp() throws SQLException
	{
			candidateDao.cleanUp();
	}
}
