package beans;
import java.util.List;
import dao.BankAccountDaoImpl;
import pojos.BankAccount;

public class BankAccountBean {
	//add a property to represent vendor id
	private int vendorId;
	//dao
	private BankAccountDaoImpl acctDao;
	
	public BankAccountBean() {
		System.out.println("in acct bean constr");
		//create dao instance
		acctDao=new BankAccountDaoImpl();
	}
	//setter
	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}
	//add B.L method to fetch all accts for logged in vendor
	public List<BankAccount> fetchAccounts()
	{
		//invoke acct dao's method
		return acctDao.getAllAccountsByVendorId(vendorId);
	}
	
}
