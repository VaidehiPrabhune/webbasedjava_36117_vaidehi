package dao;
import static utils.HibernateUtils.getSf;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojos.BankAccount;
import pojos.Vendor;

public class BankAccountDaoImpl implements IBankAccountDao {

	@Override
	public List<BankAccount> getAllAccountsByVendorId(int vendorId) {
		List<BankAccount> accts=null;
		String jpql="select a from BankAccount a where a.accountOwner.vendorId=:vid";
		Session session =getSf().getCurrentSession();
		//begin tx
		Transaction tx=session.beginTransaction();
		try {
			accts = session.createQuery(jpql, BankAccount.class).setParameter("vid", vendorId).getResultList();
			tx.commit();
		}catch(RuntimeException e)
		{
			if(tx != null)
				tx.rollback();
			throw e;
		}
		return accts;
	}

}
