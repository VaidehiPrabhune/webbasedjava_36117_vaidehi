package dao;

import java.util.List;

import pojos.BankAccount;

public interface IBankAccountDao {
//add method to list all bank accounts for a vendor
	List<BankAccount> getAllAccountsByVendorId(int vendorId);
}
