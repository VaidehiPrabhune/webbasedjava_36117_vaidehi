<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<%--acct_bean.setVendorId(session.getAttribute("vendor_bean").getValidatedDetails().getVendorId()) --%>
<jsp:setProperty property="vendorId"
	value="${sessionScope.vendor_bean.validatedDetails.vendorId}"
	name="acct_bean" />
<body>
	<%--display login successful message using EL syntax --%>
	<h5>${sessionScope.vendor_bean.message}</h5>
	<h5>Vendor Details :${sessionScope.vendor_bean.validatedDetails}</h5>
	<h5 align="center">A/C summary</h5>
	${sessionScope.acct_bean.fetchAccounts()}
</body>
</html>