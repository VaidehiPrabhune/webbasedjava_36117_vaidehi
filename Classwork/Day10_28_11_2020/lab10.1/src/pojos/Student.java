package pojos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "students_tbl")
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "student_id")
	private Integer studentId;
	@Column(length = 20, unique = true)
	private String email;
	@Column(length = 20)
	private String name;
	// bi dir asso between entities
	// many side of the asso. n owning side (since it has FK column)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "c_id", nullable = false) // constraint : NOT NULL : Optional BUT recommended
	private Course selectedCourse;
	// one to one association bet entities :inverse
	@OneToOne(mappedBy = "stud", cascade = CascadeType.ALL)
	private Address studentAdr;
	// one to one association bet entity and value type
	@Embedded // optional :added for understanding purpose
	private AdharCard card;
	// one to many association bet entity n collection of value types
	// :unidirectional
	@ElementCollection //mandatory
	@CollectionTable(name = "hobbies_tbl",joinColumns = @JoinColumn(name ="s_id")) //optional but recommonded
	@Column(name ="hobby",length = 20)
	private List<String> hobbies=new ArrayList<String>();
	//one to many association between entity n value type : uni dir (entity--->value type)
	@ElementCollection
	@CollectionTable(name ="edu_qualifications",joinColumns = @JoinColumn(name="student_id"))
	private List<EducationalQualifications> qualifications=new ArrayList<>();
	
	public Student() {
		System.out.println("in student cnstr");
	}

	public Student(String email, String name) {
		super();
		this.email = email;
		this.name = name;
	}
	public List<EducationalQualifications> getQualifications() {
		return qualifications;
	}

	public void setQualifications(List<EducationalQualifications> qualifications) {
		this.qualifications = qualifications;
	}
	public List<String> getHobbies() {
		return hobbies;
	}

	public void setHobbies(List<String> hobbies) {
		this.hobbies = hobbies;
	}

	public AdharCard getCard() {
		return card;
	}

	public void setCard(AdharCard card) {
		this.card = card;
	}

	// all s/g
	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Course getSelectedCourse() {
		return selectedCourse;
	}

	public void setSelectedCourse(Course selectedCourse) {
		this.selectedCourse = selectedCourse;
	}

	public Address getStudentAdr() {
		return studentAdr;
	}

	public void setStudentAdr(Address studentAdr) {
		this.studentAdr = studentAdr;
	}

	// add helper method to assign address
	public void addAddress(Address a) {
		// p->c
		this.studentAdr = a;
		a.setStud(this);
	}

	// add helper method to remove address
	public void removeAddress(Address a) {
		// c->p
		this.studentAdr = null;
		a.setStud(null);
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", email=" + email + ", name=" + name + "]";
	}

}
