package pojos;

import java.time.LocalDate;
import javax.persistence.*;

@Embeddable // Mandatory : required to tell hibernate :value type : which doesnt have
			// standalone existance
//n its details should be embedded in owning entity
public class AdharCard {
	@Column(name = "card_number", length = 20, unique = true)
	private String cardNumber;
	@Column(length = 40)
	private String location;
	@Column(name = "created_on")
	private LocalDate createdOn;

	public AdharCard() {
		System.out.println("in adhar card constructor");
	}

	public AdharCard(String cardNumber, String location, LocalDate createdOn) {
		super();
		this.cardNumber = cardNumber;
		this.location = location;
		this.createdOn = createdOn;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public LocalDate getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDate createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String toString() {
		return "AdharCard [cardNumber=" + cardNumber + ", location=" + location + ", createdOn=" + createdOn + "]";
	}

}
