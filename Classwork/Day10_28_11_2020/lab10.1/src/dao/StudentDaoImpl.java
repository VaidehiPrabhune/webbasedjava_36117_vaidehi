package dao;

import static utils.HibernateUtils.getSf;
import org.hibernate.*;

import pojos.Address;
import pojos.Course;
import pojos.Student;

public class StudentDaoImpl implements IStudentDao {

	@Override
	public String cancelStudentAdmission(String email1, String courseName1) {
		String mesg = "Cancelling admisssion failed ...";
		String jpqlStudent = "select s from Student s where s.email=:email";
		String jpqlCourse = "select c from Course c where c.name=:nm";
		// session
		Session session = getSf().getCurrentSession();
		// tx
		Transaction tx = session.beginTransaction();
		try {
			// get student details from email
			Student s = session.createQuery(jpqlStudent, Student.class).setParameter("email", email1).getSingleResult();
			// s : PERSISTENT
			// get course details from it's name
			Course c = session.createQuery(jpqlCourse, Course.class).setParameter("nm", courseName1).getSingleResult();
			// c : PERSISTENT
			c.removeStudent(s);// helper method to de link bi dir association between course n student
			tx.commit();
			mesg = s.getName() + "'s admission cancelled....";
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();// session closed
			// --db cn rets to the pool , L1 cache is destroyed
			throw e;
		}
		return mesg;
	}

	@Override
	public Student getStudentDetails(int studentId) {
		Student s=null;
		Session session=getSf().getCurrentSession();
		//begin tx
		Transaction tx=session.beginTransaction();
		try {
			s=session.load(Student.class,studentId);
			//invoke non id getter to trigger select query
			s.getName();//trigger 
			tx.commit();
		} catch (RuntimeException e) {
			if(tx != null)
				tx.rollback();
			throw e;
		}
		return s;
	}
	// Assign address to the existing student
		@Override
		public String assignAddressToStudent(String email,Address address)
		{
			String mesg="assigning address failed...";
			String jpql="select s from Student s where s.email=:em";
			Session session=getSf().getCurrentSession();
			//begin tx
			Transaction tx=session.beginTransaction();
			try {
				Student	s=session.createQuery(jpql, Student.class).setParameter("em", email).getSingleResult();
				//s:persistent
				s.addAddress(address);
				tx.commit();
				mesg="Address linked to student : "+s.getName();
			} catch (RuntimeException e) {
				if(tx != null)
					tx.rollback();
				throw e;
			}
			return mesg;
		}

		@Override
		public Student fetchCompleteStudentDetails(String email) {
			Student s=null;
			String jpql="select s from Student s join fetch s.selectedCourse join fetch s.studentAdr where s.email=:em";
			Session session=getSf().getCurrentSession();
			//begin tx
			Transaction tx=session.beginTransaction();
			try {
				s=session.createQuery(jpql, Student.class).setParameter("em", email).getSingleResult();			
				tx.commit();
				
			} catch (RuntimeException e) {
				if(tx != null)
					tx.rollback();
				throw e;
			}
			return s;
			
		}

}
