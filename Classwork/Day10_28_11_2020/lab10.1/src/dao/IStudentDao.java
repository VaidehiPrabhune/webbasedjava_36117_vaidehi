package dao;

import pojos.Address;
import pojos.Student;

public interface IStudentDao {
	String cancelStudentAdmission(String email, String courseName);

	// fetch student details
	Student getStudentDetails(int studentId);

	// Assign address to the existing student
	String assignAddressToStudent(String email,Address address);
	
	//fetch complete student details
	Student fetchCompleteStudentDetails(String email);
}
