package dao;

import pojos.Course;

public interface ICourseDao {
	//add a method to launch new course
	String launchCourse(Course c);
	//cancel existing course
	String cancelCourse(int courseId);
	//get course details
	Course getCourseDetails(String courseName);
	//get course and associated student details:by accessing size :uses 2 queries
	Course getCompleteCourseDetails(String courseName);
	//get course n associated student details :using single join query
	Course getCompleteCourseDetailsWithJoin(String courseName); 
	
}
