package dao;

import pojos.Course;
import static utils.HibernateUtils.getSf;
import org.hibernate.*;

public class CourseDaoImpl implements ICourseDao {

	@Override
	public String launchCourse(Course c) {
		String mesg = "Launching course failed...";
		// session
		Session session = getSf().getCurrentSession();
		// tx
		Transaction tx = session.beginTransaction();
		try {
			// c : transient
			session.persist(c);// persistent
			tx.commit();// dirty chking : insert , session closed
			mesg = "Launched course with course id " + c.getCourseId();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return mesg;
	}

	@Override
	public String cancelCourse(int courseId) {
		String mesg = "Course cancellation failed....";
		// session
		Session session = getSf().getCurrentSession();
		// tx
		Transaction tx = session.beginTransaction();
		try {
			// get course details from course id
			Course c = session.get(Course.class, courseId);
			if (c != null) {
				// delete course details
				// c : PERSISTENT
				session.delete(c);// c : REMOVED (not yet gone from L1 cache or DB) : simply marked for removal
				mesg = "Course with name " + c.getName() + " cancelled....";
			}
			tx.commit();// delete query
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();// session closed
			// --db cn rets to the pool , L1 cache is destroyed
			throw e;
		}
		return mesg;
	}

	@Override
	public Course getCourseDetails(String courseName) {
		String jpql = "select c from Course c where c.name = :nm";
		Course c = null;
		// session
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			c = session.createQuery(jpql, Course.class).setParameter("nm", courseName).getSingleResult();
			// c : persistent
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return c; // c : detached
	}

	@Override
	public Course getCompleteCourseDetails(String courseName) {
		String jpql = "select c from Course c where c.name = :nm";
		Course c = null;
		// session
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			c = session.createQuery(jpql, Course.class).setParameter("nm", courseName).getSingleResult();
			// c : persistent
			//Hint :access the size of the collection within session scope
			c.getStudents().size();//another select query will be fired on student table :using FK course id
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return c; // c : detached
	}

	@Override
	public Course getCompleteCourseDetailsWithJoin(String courseName) {
		String jpql = "select c from Course c left outer join fetch c.students where c.name = :nm";
		Course c = null;
		// session
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			c = session.createQuery(jpql, Course.class).setParameter("nm", courseName).getSingleResult();
			// c : persistent						
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return c; // c : detached
		
	}

}
