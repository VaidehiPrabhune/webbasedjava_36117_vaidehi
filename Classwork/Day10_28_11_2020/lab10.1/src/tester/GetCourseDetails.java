package tester;
import static utils.HibernateUtils.getSf;

import java.util.Scanner;

import org.hibernate.*;

import dao.CourseDaoImpl;
import pojos.Course;
import static java.time.LocalDate.parse;
public class GetCourseDetails {
	public static void main(String[] args) {
		// Testing bootstrapping of hibernate configuration (creating singleton n
		// immutable instance of SessionFactory (SF)
		try (SessionFactory sf = getSf(); Scanner sc = new Scanner(System.in)) {
			CourseDaoImpl courseDao = new CourseDaoImpl();
			System.out
					.println("Enter course name to view the details");
			Course c=courseDao.getCourseDetails(sc.next());
			System.out.println("Course Details ");
			System.out.println(c);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
