package dao;

import pojos.Role;
import pojos.User;

import org.apache.commons.io.FileUtils;
import org.hibernate.*;
import static utils.HibernateUtils.getSf;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class UserDaoImpl implements IUserDao {

	@Override
	public String registerUser(User user) {
		String mesg = "User reg failed...";
		// user : TRANSIENT (not in L1 cache n not in DB ) :exists only in java heap
		// 1 : get session from SF : getCurrentSession
		Session session = getSf().getCurrentSession();
		Session session2 = getSf().getCurrentSession();
		System.out.println(session == session2);// true
		// begin tx
		Transaction tx = session.beginTransaction(); // db cn is pooled out n wrapped in session n reted to the caller
		System.out.println("after begin tx : session open " + session.isOpen() + " conn " + session.isConnected());// true
																													// true
		// EMPTY L1 cache is created
		try {
			// insert new user's info
			Integer id = (Integer) session.save(user);// user : PERSISTENT (only added in L1 cache : not yet part of DB)
			System.out.println("generated id " + id);
			tx.commit();// Hibernate performs : auto dirty checking : insert query : to synch state of
						// L1 cache with that of DB , session implicitly closed => db cn rets to the
						// pool n L1 cache is destroyed.
			mesg = "User registered with ID " + id;
			System.out
					.println("after commit  tx : session open " + session.isOpen() + " conn " + session.isConnected());// false
																														// false

		} catch (HibernateException e) {
			// rollback tx n re throw the exc to the caller
			if (tx != null)
				tx.rollback();// session implicitly closed => db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		System.out.println("before ret   : session open " + session.isOpen() + " conn " + session.isConnected());// false
																													// false

		return mesg;// user : DETACHED
	}

	@Override
	public User fetchUserDetails(int userId) {
		User u = null;// u : Not applicable
		// session
		Session session = getSf().getCurrentSession();
		// tx
		Transaction tx = session.beginTransaction();
		try {
			u = session.get(User.class, userId);// int --->Integer (auto boxing) ---> Serializable (up casting)
			// u : in case of valid id , u : PERSISTENT (part of L1 cache)
			u = session.get(User.class, userId);// from cache
			u = session.get(User.class, userId);// from cache
			tx.commit();// auto dirty chking : no queries , db cn rets to the pool n L1 cache is
						// destroyed.
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}

		return u;// u : DETACHED
	}

	@Override
	public List<User> fetchAllUserDetails() {
		String jpql = "select u from User u";
		List<User> users = null;// users : null
		// get session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			// create query from Session , execute the same
			users = session.createQuery(jpql, User.class).getResultList();
			// users=session.createQuery(jpql, User.class).getResultList();
			// users=session.createQuery(jpql, User.class).getResultList();
			// users : list of PERSISTENT pojos
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		return users;// users : list of DETACHED pojos
	}

// Display all users registered between strt date n end date & under a specific role
	@Override
	public List<User> fetchSelectedUserDetails(Date strtDate, Date endDate, Role userRole) {
		List<User> users = null;
		String jpql = "select u from User u where u.regDate between :start and :end and u.role=:rl";
		// get session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, User.class).setParameter("start", strtDate).setParameter("end", endDate)
					.setParameter("rl", userRole).getResultList();
			// users : list of persistent pojos/entities
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		return users;
	}

	@Override
	public List<String> fetchSelectedUserNames(Date strtDate, Date endDate, Role userRole) {
		List<String> users = null;
		String jpql = "select u.name from User u where u.regDate between :start and :end and u.role=:rl";
		// get session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, String.class).setParameter("start", strtDate).setParameter("end", endDate)
					.setParameter("rl", userRole).getResultList();
			// users : list of persistent pojos/entities
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		return users;

	}

	@Override
	public String changePassword(String email, String oldPwd, String newPwd) {
		String mesg = "Chaging password failed....";
		String jpql = "select u from User u where u.email=:em and u.password=:pass";// select jpql : for authentication
		// get hib session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		User u = null;
		try {
			// create query , set IN params , get single result
			u = session.createQuery(jpql, User.class).setParameter("em", email).setParameter("pass", oldPwd)
					.getSingleResult();
			// in case of valid credentials : getSingleResult() rets : PERSISTENT POJO ref
			// no null chking required : since : throws exc in case of no res found
			// u : PERSISTENT : exist in L1 cache , exists in DB
			// session.evict(u);//u:detached
			session.clear();// clears entire L1 cache (i.e all the persistent are entities are unbound from
							// L1 cache)
			u.setPassword(newPwd);// abcd : modifying state of PERSISTENT POJO
			tx.commit();// hib performs auto dirty chking : detects a change : update , session closed
						// --db cn rets to the pool , L1 cache is destroyed
			// mesg = "Updated password...";
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();// session closed
			// --db cn rets to the pool , L1 cache is destroyed
			throw e;
		}
		// u : DETACHED : hibnernate DOES NOT propagate the changes done to state of
		// detached pojo ---> DB
		u.setPassword(newPwd.toUpperCase());// ABCD
		return mesg;
	}

	// add method to Un subscribe user :i/p :email n password
	@Override
	public String unsubscribeUser(String email, String password) {
		String mesg = "User un subscription failed...";
		// add jpql : to authenticate user
		String jpql = "select u from User u where u.email=:em and u.password=:pass";
		// session
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			// validate user
			User u = session.createQuery(jpql, User.class).setParameter("em", email).setParameter("pass", password)
					.getSingleResult();
			// u:PERSISTENT pojo
			// session API :public void delete(Object o)
			session.delete(u);// u : is marked for removal,neither gone from L1 cache nor DB
			tx.commit();// performs dirty checking:delete query ,session is closed : db conn return to
						// pool and L1 cache is destroyed.
			// entity is removed from cache as well
			mesg = "User " + u.getName() + " un subscribed...";
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();// session closed
			// --db cn rets to the pool , L1 cache is destroyed
			throw e;
		}
		// u : TRANSIENT (not in db ,not in L1 cache)exist only in java heap
		return mesg;
	}// user object is marked for garbage collection

	@Override
	public String bulkUpdateUsers(Date date, double discount) {
		String mesg = "bulk updation failed...";
		// 1.update query in jpql
		String jpql = "update User u set u.regAmount=u.regAmount-:disc where u.regDate<:dt";
		// session
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			int updateCount = session.createQuery(jpql).setParameter("disc", discount).setParameter("dt", date)
					.executeUpdate();

			tx.commit();// update query , empty L1 cache is destroyed , cn rets to the pool.
			mesg = updateCount + " users updated...";
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();// session closed
			// --db cn rets to the pool , L1 cache is destroyed
			throw e;
		}
		return mesg;

	}

	@Override
	public String testSessionAPI(User user) {
		String mesg = "User reg failed...";
		// user : TRANSIENT (not in L1 cache n not in DB ) :exists only in java heap
		// 1 : get session from SF : getCurrentSession
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction(); // db cn is pooled out n wrapped in session n reted to the caller
		// EMPTY L1 cache is created
		try {
			// insert new user's info
			session.saveOrUpdate(user);// user : PERSISTENT (only added in L1 cache : not yet part of DB)

			tx.commit();// Hibernate performs : auto dirty checking : insert query : to synch state of
						// L1 cache with that of DB , session implicitly closed => db cn rets to the
						// pool n L1 cache is destroyed.
			mesg = "User registered with ID " + user.getUserId();
		} catch (RuntimeException e) {
			// rollback tx n re throw the exc to the caller
			if (tx != null)
				tx.rollback();// session implicitly closed => db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		return mesg;// user : DETACHED
	}

	@Override
	public String saveImage(int userId, String fileName) throws IOException {
		String mesg="Saving image failed...";
		//validate file : chk if it's readable existing data file
		//create instance of java.io.File
		File file=new File(fileName);
		if(file.exists() && file.isFile() && file.canRead())
		{
		//valid file			
		// session
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			//get user details from user id
			User user=session.get(User.class, userId);
			//null checking
			if(user != null)
			{
				//user : PERSISTANT 
				//Method below  : reads binary file & returns contents it byte[] n close file
				user.setImage(FileUtils.readFileToByteArray(file));//modifying state of persistent pojo 
				
			}
			tx.commit();// hibernate auto dirty chking : update query : close session --cn rets to pool
			mesg="Image save in db....";
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();// session closed
			// --db cn rets to the pool , L1 cache is destroyed
			throw e;
		  }
		}
		return mesg;

	}

}
