package dao;

import pojos.Course;
import static utils.HibernateUtils.getSf;
import org.hibernate.*;

public class CourseDaoImpl implements ICourseDao {

	@Override
	public String launchCourse(Course c) {
		String mesg = "Launching course failed";
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			// c:TRANSIENT
			session.persist(c);// C:PERSISTENT
			tx.commit();// dirty checking :insert,session closed,DB return L1 cache destroyed
			mesg = "Launched course with course id" + c.getCourseId();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();// session closed
			// --db cn rets to the pool , L1 cache is destroyed
			throw e;
		}
		return mesg;
	}

	@Override
	public String cancelCourse(int courseId) {
		String mesg = "Launching cancellation failed....";
		// session
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			//get couse details from course ID
			Course c=session.get(Course.class, courseId);
			if( c!= null)
			{
				//delete course details
				//c:PERSISTENT
				session.delete(c);//c:REMOVED(not yet gone from L1 cache or Db): it is simply marked for removal
				mesg = "Course with name"+c.getName()+" cancelled...";
			}
			tx.commit();//delete query fired
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();// session closed
			// --db cn rets to the pool , L1 cache is destroyed
			throw e;
		}
		return mesg;
	}

}
