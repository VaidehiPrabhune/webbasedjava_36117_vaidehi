package dao;

import static utils.HibernateUtils.getSf;
import org.hibernate.*;

import pojos.Course;
import pojos.Student;

public class StudentDaoImpl implements IStudentDao {

	@Override
	public String cancelStudentAdmission(String email, String courseName) {
		String mesg="Cancelling admission failed...";
		String jpqlStudent="select s from Student s where s.email=:email";
		String jpqlCourse="select c from Course c where c.name=:nm";
		// session
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			//get student details from email
			Student s=session.createQuery(jpqlStudent, Student.class).setParameter("email", email).getSingleResult();
			//s : PERSISTANT 
			//get course details from its name
			Course c=session.createQuery(jpqlCourse, Course.class).setParameter("nm", courseName).getSingleResult();
			//c:PERSISTENT
			c.removeStudent(s);//helper method called to de link bidirectional association between course n student
			tx.commit();
			mesg=s.getName()+"s admission cancelled..";
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();// session closed
			// --db cn rets to the pool , L1 cache is destroyed
			throw e;
		}
		return mesg;
	}

}
