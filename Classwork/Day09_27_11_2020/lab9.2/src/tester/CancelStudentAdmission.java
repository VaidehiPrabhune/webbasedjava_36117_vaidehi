package tester;
import static utils.HibernateUtils.getSf;
import static java.time.LocalDate.parse;
import java.util.Scanner;

import org.hibernate.*;

import dao.CourseDaoImpl;
import dao.StudentDaoImpl;
import pojos.Course;

public class CancelStudentAdmission {

	public static void main(String[] args) {
		// Testing bootstrapping of hibernate configuration (creating singleton n
		// immutable instance of SessionFactory (SF)
		try(SessionFactory sf=getSf();Scanner sc=new Scanner(System.in))
		{
			StudentDaoImpl studentDao=new StudentDaoImpl();
			System.out.println("Enter student email n course name,to cancel the admission");
		System.out.println("status"+studentDao.cancelStudentAdmission(sc.next(), sc.next()));
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

}
