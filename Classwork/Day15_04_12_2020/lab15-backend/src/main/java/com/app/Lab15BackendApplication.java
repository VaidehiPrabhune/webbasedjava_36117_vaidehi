package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab15BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lab15BackendApplication.class, args);
	}

}
