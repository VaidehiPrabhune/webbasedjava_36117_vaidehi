package com.app.controller;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Role;
import com.app.pojos.Vendor;
import com.app.service.IVendorService;

@Controller // Mandatory
@RequestMapping("/user") // optional BUT recommended
public class UserController {
	// dependency : service layer
	@Autowired
	private IVendorService vendorService;

	public UserController() {
		System.out.println("in ctor of " + getClass().getName() + " " + vendorService);// null

	}

	@PostConstruct
	public void myInit() {
		System.out.println("in user controller's init " + vendorService);// not null
	}

	// add req handling method : get : to show login form
	@GetMapping("/login")
	public String showLoginForm() {
		System.out.println("in show login form");
		return "/user/login";// actual view name : /WEB-INF/views/user/login.jsp
	}

	// add req handling method with method=post : to process the form
	@PostMapping("/login")
	public String processLoginForm(@RequestParam String email, @RequestParam String password, Model map,
			HttpSession session, RedirectAttributes flashMap) {
		// RedirectAttributes : i/f => used in redirect scenario, to store the
		// attributes , till the next request
		// String email=request.getParamter("email");...
		System.out.println("in process login form " + email + " " + password);
		// invoke service layer method for exec B.L
		try {
			Vendor validatedUser = vendorService.authenticateUser(email, password);
			// => valid login
			// add validated user details under session scope
			session.setAttribute("user_details", validatedUser);
			// scope : till the next request
			flashMap.addFlashAttribute("message", "Login Successful : " + validatedUser.getUserRole());
			// chk the role n redirect accordingly
			if (validatedUser.getUserRole().equals(Role.ADMIN))
				return "redirect:/admin/list";// redirect view name :
			// controller sends redirect view name to D.S
			// D.S : skips the V.R & invokes :
			// response.sendRedirect(response.encodeRedirectURL("/admin/list"));
			// WC : sends redirect resp pkt : SC 302 | location : /admin/list + jsessionid |
			// body : empty
			// clnt brower sends next request :
			// http://host:port/day14/admin/list;jsessionid=g7567gh,
			// method=get

			return "redirect:/vendor/details";// to avoid double submit issue : replace server pull by client pull
			// SC :
		} catch (RuntimeException e) {
			System.out.println("err in process login form " + e);// NoResultExc
			// in case of failure : FORWARD client to login form : highlighted with error
			// message
			map.addAttribute("message", "Invalid Login , Pls retry....");
			return "/user/login";// actual view name : /WEB-INF/views/user/login.jsp

		}

	}

	// add request handling method for user's logout
	@GetMapping("/logout")
	public String userLogout(HttpSession hs, Model modelAttrMap, HttpServletResponse response,
			HttpServletRequest request) {
		System.out.println("in user's logout");
		// get user details from HttpSession n add it the model map
		modelAttrMap.addAttribute("details", hs.getAttribute("user_details"));
		// discard HtpSession
		hs.invalidate();
		// To navigate the clnt from logout page to index page : after a dly :
		// set Http response header : name : refresh ,
		// value=10;url=http://host:port/day14_boot : context path
		System.out.println("ctx path " + request.getContextPath());//   /day14_boot
		response.setHeader("refresh", "5;url=" + request.getContextPath());
		//resp pkt : SC 200 | refresh : location /day14_boot | body contents : non EMPTY(hello user name , logout mesg)
		//after dly of 5 sec : clnt browser : generates a new req.

		return "/user/logout";// user controller ---> D.S : actual view name : /WEB-INF/views/user/logout.jsp
	}
}
