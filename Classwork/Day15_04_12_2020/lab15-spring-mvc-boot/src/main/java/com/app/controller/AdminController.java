package com.app.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Role;
import com.app.pojos.Vendor;
import com.app.service.IVendorService;

@Controller
@RequestMapping("/admin")
public class AdminController {
	//dependency : vendor service layer
	@Autowired
	private IVendorService vendorService;
	public AdminController() {
		System.out.println("in ctor of "+getClass().getName());
	}
	//add request handling method to show vendor list
	@GetMapping("/list")
	public String showVendorList(Model map)
	{
		System.out.println("in show vendor list "+map);
		// save current vendor list under model map --> auto saved under curnt request scope
		map.addAttribute("vendor_list",vendorService.listAllVendors()); 
		return "/admin/list";//actual view name reted by V.R : /WEB-INF/views/admin/list.jsp
	}
	//add req handling method to delete vendor details
	@GetMapping("/delete")
	public String removeVendorDetails(@RequestParam int vid,RedirectAttributes flashMap)
	{
		System.out.println("in remove vendor details "+vid);
		//invoke service layer method
		flashMap.addFlashAttribute("message", vendorService.deleteVendorDetails(vid));
		//client pull : redirect scenario
		return "redirect:/admin/list";
	}
	//add req handling method to show vendor registration form
	@GetMapping("/register")
	public String showRegForm(Vendor v)
	{
		System.out.println("in show reg form");
		//add empty vendor POJO instance in model map
		//map.addAttribute("vendor_dtls", new Vendor());
		return "/admin/register";//actual view name : /WEB-INF/views/admin/register.jsp
	}
	//add req handling method to process vendor registration form
	@PostMapping("/register")
	public String processRegForm(@Valid Vendor v,BindingResult result,RedirectAttributes flashMap)
	{
		//S.C :by def  :modelMap.getAttribute("vendor")--empty POJO--setters+apply validation rules --populated transient POJO
		//results of data binding is stored under BindingResult
		System.out.println("in process reg form "+v);//v : TRANSIENT
		v.setUserRole(Role.VENDOR);
		//chk for presentation logic errors
		if(result.hasErrors())
		{
			System.out.println("P.L errors in Binding Result "+result);
			//navigate the client back to reg form:highlighted with errors
			//forward :server pull
			return "/admin/register";
		}
		System.out.println("No P.L errors..continuing with B.L");
		flashMap.addFlashAttribute("message",vendorService.registerNewVendor(v));
		return "redirect:/admin/list";//redirect view name
	}

}
