package com.app.pojos;

import java.time.LocalDate;

public class Product {
	private Integer productId;
	private String productName;
	private double price;
	private String productDesc;
	private LocalDate expDate;
	public Product(Integer productId, String productName, double price, LocalDate expDate) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.price = price;
		this.expDate = expDate;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getProductDesc() {
		return productDesc;
	}
	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}
	public LocalDate getExpDate() {
		return expDate;
	}
	public void setExpDate(LocalDate expDate) {
		this.expDate = expDate;
	}
	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", price=" + price
				+ ", productDesc=" + productDesc + ", expDate=" + expDate + "]";
	}

	
}
