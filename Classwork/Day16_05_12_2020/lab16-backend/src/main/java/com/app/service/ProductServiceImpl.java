package com.app.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.custom_exception.ProductNotFoundException;
import com.app.dao.ProductRepository;
import com.app.pojos.Product;

@Service //mandatory
@Transactional //optional since it is by default already added by JPA repo
public class ProductServiceImpl implements IProductService {
	//dependancy
	@Autowired
	private ProductRepository productRepo;
	@Override
	public List<Product> getAllProducts() {
		// TODO Auto-generated method stub
		return productRepo.findAll();
	}
	@Override
	public Product getProductDetails(int productId) {
		//invoke daos method
		Optional<Product> optionalProduct = productRepo.findById(productId);
		if(optionalProduct.isPresent())
			return optionalProduct.get();
		//if product not found : throw custom exception
		throw new ProductNotFoundException("Product not found :INvalid Id"+productId);
	}
	@Override
	public Product getProductDetailsByName(String pName) {
		//invoke repo's method
		Optional<Product> optional = productRepo.findByName(pName);
		if(optional.isPresent())
			return optional.get();
		//if product not found : throw custom exception
		throw new ProductNotFoundException("Product not found :INvalid name"+pName);
		
	}
	@Override
	public Product addProductDetails(Product p) {		
		return productRepo.save(p);
	}//auto dirty checking  -- insert query -- L1 cache destroyed  -- conn returs cn pool --persistence context is closed
	//returns detached pojo to caller (REST controller)
	
	@Override
	public Product updateProductDetails(Product p) {
		//chk if product exists
		Optional<Product> optional=productRepo.findById(p.getProductId());
		if(optional.isPresent())
			return productRepo.save(p);//update
		//if product not found : throw custom exception
		throw new ProductNotFoundException("Product not found :INvalid Id"+p.getProductId());
	}//auto dirty checking  -- update query -- L1 cache destroyed  -- conn returs cn pool --persistence context is closed
	//returns detached pojo to caller (REST controller)
	
	
	@Override
	public void deleteProductDetails(int productId) {
	//chk product exists : if yes : delete ,otherwise throw exception
		Optional<Product> optional=productRepo.findById(productId);
		if(optional.isPresent())
			productRepo.deleteById(productId);//update
		//if product not found : throw custom exception
		throw new ProductNotFoundException("Product not found :INvalid Id"+productId);

		
	}
}
