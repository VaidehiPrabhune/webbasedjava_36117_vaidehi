package com.app.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.Product;

public interface ProductRepository extends JpaRepository<Product,Integer>{
	//List product details having supplied name
	Optional<Product> findByName(String productName);
	//list all products containing supplied keyword in desc 
	List<Product> findByProductDescContaining(String pattern); 
	//List all products with date of expiry after a specific date
	List<Product> findByExpDateGreaterThan(LocalDate date);
	//List of products with price in the range of begin n end
	List<Product> findByPriceBetween(double begin,double end);
}
