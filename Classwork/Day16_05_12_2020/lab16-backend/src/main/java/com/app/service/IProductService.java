package com.app.service;

import java.util.List;

import com.app.pojos.Product;

public interface IProductService {
//add a method to list all the products
	List<Product> getAllProducts();
	//add a method to get specific product details by its id
	Product getProductDetails(int productId);
	//get product list by supplied name
	Product getProductDetailsByName(String pName);
	//save or add new product details
	Product addProductDetails(Product p);//p:TRANSIENT
	//update new product details
	Product updateProductDetails(Product p); //p:DETACHED
	//delete product by supplied id
	void deleteProductDetails(int productId);
	
}
