package custom_tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class HelloTag extends SimpleTagSupport{
	public HelloTag() {
		System.out.println("in hello tag constr");
	}

	@Override
	public void doTag() throws JspException, IOException {
		System.out.println("in do-tag");
		//send hello mesg to clnt side. : out (JspWriter).print
		//holder of all implicit : PageContext
		//WC invokes setJspContext on tag handler's instance : setter Based D.I : PageContext
		//doTag
		getJspContext().getOut().print("Hello from custom tags...");	
	}
	
}
