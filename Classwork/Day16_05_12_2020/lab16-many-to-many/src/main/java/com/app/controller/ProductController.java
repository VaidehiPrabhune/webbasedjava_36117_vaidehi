package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Product;
import com.app.service.IProductService;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	@Autowired
	private IProductService productService;
	
	public ProductController() {
		System.out.println("in ctor of  " + getClass().getName());
	}
	
	
	//add a req handlling method to return representation of list of avail products
	@GetMapping
	public ResponseEntity<?> fetchAllProducts()
	{
		System.out.println("in fetch all products..");

		//return new ResponseEntity<>(productService.getAllProducts(),HttpStatus.OK);	}
		List<Product> products = productService.getAllProducts();
		
		//check if empty
		if(products.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		
		return new ResponseEntity<>(products, HttpStatus.OK);
	}
	
	//add a req handlling method to return representation of selected product by id 
	//OR in case of invalid id send SC 404
	@GetMapping("/{pid}")
	public ResponseEntity<?> getProductDetails(@PathVariable int pid)
	{
		System.out.println(" in get product dtls "+pid);
		try {
			return ResponseEntity.ok(productService.getProductDetails(pid));
		}catch (RuntimeException e) {
			System.out.println("er in controller "+e);
			
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	

}