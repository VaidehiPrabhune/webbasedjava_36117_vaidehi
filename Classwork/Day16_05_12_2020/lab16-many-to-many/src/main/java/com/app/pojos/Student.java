package com.app.pojos;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="students_tbl")
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "student_id")
	private Integer studentId;
	@Column(length = 20,unique = true)
	private String email;
	@Column(length = 20)
	private String name;
	//bi dir asso between entities : many to many
	//many side of the asso. n non owning side
	@ManyToMany(mappedBy = "students")
	private Set<Course> selectedCourses=new HashSet<>();
	
	//one to one asso between entities : inverse
	@OneToOne(mappedBy = "stud",cascade = CascadeType.ALL)	
	private Address studentAdr;
	//one to one association between entity n value type
	@Embedded //optional : added only for understanding purpose
	private AdharCard card;
	//one to many asso between entity n collection of value types : uni dir.
	@ElementCollection //mandatory
	@CollectionTable(name = "hobbies_tbl",joinColumns = @JoinColumn(name="s_id")) //optional BUT reco.
	@Column(name="hobby",length = 20)
	private List<String> hobbies=new ArrayList<>();
	//one to many association between entity n value type : uni dir (entity--->value type)
	@ElementCollection
	@CollectionTable(name="edu_qualifications",joinColumns = @JoinColumn(name="student_id"))
	private List<EducationalQualifications> qualifications=new ArrayList<>();
	
	public Student() {
		System.out.println("in student cnstr");
	}
	public Student(String email, String name) {
		super();
		this.email = email;
		this.name = name;
	}
	//all s/g
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public Set<Course> getSelectedCourses() {
		return selectedCourses;
	}
	public void setSelectedCourses(Set<Course> selectedCourses) {
		this.selectedCourses = selectedCourses;
	}
	public Address getStudentAdr() {
		return studentAdr;
	}
	public void setStudentAdr(Address studentAdr) {
		this.studentAdr = studentAdr;
	}
	
	public AdharCard getCard() {
		return card;
	}
	public void setCard(AdharCard card) {
		this.card = card;
	}
	
	public List<String> getHobbies() {
		return hobbies;
	}
	public void setHobbies(List<String> hobbies) {
		this.hobbies = hobbies;
	}
	
	public List<EducationalQualifications> getQualifications() {
		return qualifications;
	}
	public void setQualifications(List<EducationalQualifications> qualifications) {
		this.qualifications = qualifications;
	}
	//add helper method to assign address
	public void addAddress(Address a)
	{
		//p ---> c
		this.studentAdr=a;
		a.setStud(this);//c ---> p
	}
	public void removeAddress(Address a)
	{
		this.studentAdr=null;
		a.setStud(null);
	}
	
	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", email=" + email + ", name=" + name + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}
	
	
	

}
