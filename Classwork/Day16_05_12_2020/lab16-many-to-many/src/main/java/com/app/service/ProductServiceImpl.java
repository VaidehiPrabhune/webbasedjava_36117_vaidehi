package com.app.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.custom_exception.ProductNotFoundException;
import com.app.dao.ProductRepository;
import com.app.pojos.Product;

@Service //mandatory
@Transactional //optional since it is by default already added by JPA repo
public class ProductServiceImpl implements IProductService {
	//dependancy
	@Autowired
	private ProductRepository productRepo;
	@Override
	public List<Product> getAllProducts() {
		// TODO Auto-generated method stub
		return productRepo.findAll();
	}
	@Override
	public Product getProductDetails(int productId) {
		//invoke daos method
		Optional<Product> optionalProduct = productRepo.findById(productId);
		if(optionalProduct.isPresent())
			return optionalProduct.get();
		//if product not found : throw custom exception
		throw new ProductNotFoundException("Product not found :INvalid Id"+productId);
	}

}
