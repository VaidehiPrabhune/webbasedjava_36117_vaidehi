package com.app.pojos;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/*
 * id,name(unique),capacity,strt_date,end_date,fees
+
List<Student> students;
 */
@Entity
@Table(name = "courses_tbl")
public class Course {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cid")
	private Integer courseId;
	@Column(length = 20, unique = true)
	private String name;
	private int capacity;
	@Column(name = "start_date")
	private LocalDate startDate;
	@Column(name = "end_date")
	private LocalDate endDate;
	private double fees;
	// many to many , bi dir association between 2 entities : one side of the
	// association
	// parent n owning side of the association
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	//optional BUT recommonded to specify name of the association table + name join col and inverse join col
	@JoinTable(name="courses_students",joinColumns = @JoinColumn(name="course_id"),
	inverseJoinColumns = @JoinColumn(name="student_id"))
	private Set<Student> students = new HashSet<>();

	// def constr
	public Course() {
		System.out.println("in course cnstr");
	}

	public Course(String name, int capacity, LocalDate startDate, LocalDate endDate, double fees) {
		super();
		this.name = name;
		this.capacity = capacity;
		this.startDate = startDate;
		this.endDate = endDate;
		this.fees = fees;
	}
	// add all s/g

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public double getFees() {
		return fees;
	}

	public void setFees(double fees) {
		this.fees = fees;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	// add helper methods : to support adding student details n removing student
	// details
	// Optional : Recommended
	// add student details to a course
	public void addStudent(Student s) {
		// bi dir association
		students.add(s);// adding parent --> child
		s.getSelectedCourses().add(this);// child ----> parent

	}

	// remove student details : bi dir
	public void removeStudent(Student s) {
		// bi dir association
		students.remove(s);// removing parent --> child
		s.getSelectedCourses().remove(this);// removing child ----> parent

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Course other = (Course) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Course [courseId=" + courseId + ", name=" + name + ", capacity=" + capacity + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", fees=" + fees + "]";
	}

}
