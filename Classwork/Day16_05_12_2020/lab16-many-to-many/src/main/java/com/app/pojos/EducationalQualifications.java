package com.app.pojos;

import java.time.LocalDate;
import javax.persistence.*;

@Embeddable
public class EducationalQualifications {
	@Column(name="qualification_type",length = 20,unique = true)
	private String type;
	@Column(name="passing_date")
	private LocalDate date;
	private double gpa;
	public EducationalQualifications() {
		System.out.println("in cnstr of edu quals");
	}
	public EducationalQualifications(String type, LocalDate date, double gpa) {
		super();
		this.type = type;
		this.date = date;
		this.gpa = gpa;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public double getGpa() {
		return gpa;
	}
	public void setGpa(double gpa) {
		this.gpa = gpa;
	}
	@Override
	public String toString() {
		return "EducationalQualifications [type=" + type + ", date=" + date + ", gpa=" + gpa + "]";
	}
	
}
