package listeners;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import static utils.DBUtils.createSingletonDBConnection;
import static utils.DBUtils.cleanUp;

import java.sql.SQLException;
/**
 * Application Lifecycle Listener implementation class DBConnectionManager
 *
 */
@WebListener
public class DBConnectionManager implements ServletContextListener {

    
	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent sce)  { 
        System.out.println("In context distroy");
        //close singleton instance of DB connection
        try {
			cleanUp();
		} catch (Exception e) {
			System.out.println("err in ctx destroy "+e);
		}
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent sce)  { 
        System.out.println("In context init");
        //create singleton instance of DB connection
        //how to get DB config details: via context parameter    
        //how to get context parameters:via servlet context
        //how to get servlet context: ServletContextEvent.getServletContext()
        ServletContext ctx=sce.getServletContext();
        try {
			createSingletonDBConnection(ctx.getInitParameter("drvr_cls"), ctx.getInitParameter("db_url"),
					ctx.getInitParameter("user_nm") , ctx.getInitParameter("password"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("err in ctx init"+e);
		}
        
    }
	
}
