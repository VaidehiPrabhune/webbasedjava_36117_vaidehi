package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CategoryServlet
 */
@WebServlet("/category")
public class CategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			pw.print("<h5>Succesful Login</h5>");
			// get cookies from sent from client => server using req header
			Cookie[] cookies = request.getCookies();
			// null checking
			if (cookies != null) {
				// client has sent cookie/ cookies
				for (Cookie c : cookies) {
					if (c.getName().equals("customer_dtls"))
						pw.print("<h5>Customer Details from cookie" + c.getValue() + "</h5>");
				}
			} else
				pw.print("<h5>No Cookies, Session Tracking Failed!!!!</h5>");
				//send logout link to the client
			pw.print("<h5><a href='logout'>Log Me Out</a></h5>");
		}
	}

}
