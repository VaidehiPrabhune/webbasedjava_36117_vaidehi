package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CustomerDaoImpl;
import pojos.Customer;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = "/authenticate", loadOnStartup = 1) // for eagarly loading
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	CustomerDaoImpl dao;

	/**
	 * @see Servlet#init()
	 */
	// overriding form of the method cant add any NEW or BROADER checked exception
	public void init() throws ServletException {
		// create dao instance
		try {
			dao = new CustomerDaoImpl();
		} catch (Exception e) {
			// Centralized err handlind in servlets
			// inform WC that initialization has failed:so that WC wont continue with
			// remaining life cycles of servlet
			// How :simpley throw servlet exception to WC(web container)
			throw new ServletException("err in init of" + getClass().getName(), e);

		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
			dao.cleanUp();
		} catch (Exception e) {

			// System.out.println("IN distroy of "+getClass().getName()+"err"+e);
			throw new RuntimeException("IN distroy of " + getClass().getName() + "err" + e);// exception chaining
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// set content type
		response.setContentType("text/html");
		// open printWriter
		try (PrintWriter pw = response.getWriter()) {
			// get request params:email,password
			String email = request.getParameter("em");
			String password = request.getParameter("pass");

			// servlet -> DAO's method for customer validation
			Customer customer = dao.authenticateCustomer(email, password);
			if (customer == null) // =>invalid login --send a msg a link to retry
				pw.print("<h5>Invalid Login, Please <a href='login.html'>Retry</a></h5>");
			else // =>valid login --send a mesg + customer details
			{
				pw.print("<h5>Sucessful Login, Your Details :" + customer + "from login servlet</h5>");
				// pw.flush(); WC sends the response to the clnt browser (buffer data get send)
				// In case of successful login : navigate the clnt to the next page in NEXT
				// request coming from the clnt browser
				response.sendRedirect("category");// wc SENDS immediately temp redirect resp
				// SC 302 | location = http://host:port/lab2.1/category | body EMPTY
				// WC throws IllegalStateException , if u call send Redirect after committing
				// the response.

			}

		} catch (Exception e) {
			// inform WC
			throw new ServletException("err in do-post of" + getClass().getName() + "err" + e);

		}
	}

}
