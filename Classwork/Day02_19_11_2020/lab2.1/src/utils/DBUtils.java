package utils;
import java.sql.*;

public class DBUtils {
//add a static method to return fixed DB connection
	public static Connection fetchConnection() throws ClassNotFoundException, SQLException
	{
		//load JDBC Driver
		Class.forName("com.mysql.cj.jdbc.Driver");
		//get connection from DriverManager
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/webJava_db?useSSL=false","dac","dac");
	}
	
	
}
