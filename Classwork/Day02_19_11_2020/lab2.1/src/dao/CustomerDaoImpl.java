package dao;

import pojos.Customer;
import java.sql.*;
//can directly access static method of a class
import static utils.DBUtils.fetchConnection;

public class CustomerDaoImpl implements ICustomerDao {
	//data members :INstance variables
	private Connection cn;
	private PreparedStatement pst1;
	
	//constr :arg less constr
	public CustomerDaoImpl() throws ClassNotFoundException, SQLException {
		//get connection from dbUtils
		cn=fetchConnection();
		//create pre parsed n pre compiled statement to hold validation query
		pst1=cn.prepareStatement("select * from my_customers where email=? and password=?");
		System.out.println("Customer Dao created");
	}
	
	@Override
	public Customer authenticateCustomer(String email, String pwd) throws Exception {
		//set IN params
		pst1.setString(1, email);
		pst1.setString(2, pwd);
		try(ResultSet rst=pst1.executeQuery())
		{
			if(rst.next())
				return new Customer(rst.getInt(1), email, pwd, rst.getDouble(4), rst.getDate(5));
		}
		return null;
	}
	
	//cleaning up of DB resources
	public void cleanUp() throws Exception
	{
		if(pst1 != null)
			pst1.close();
		
		if(cn != null)
			cn.close();
		System.out.println("customer dao cleaned up...");
	}

}
