package com.app.controller;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller//mandatory to tell SC whatever follows is a req handling controller bean
//SPring bean :singleton & eager
public class HelloController {
	public HelloController() {
		System.out.println("in ctor of "+getClass().getName());
	}
	
	//to tell SC about reqest handling method
	//entry in HandlerMapping bean
	//key  : /hello
	//value:com.app.controller.HelloController:sayHello()
	@RequestMapping("/hello")
	public String sayHello()
	{
		System.out.println("in say hello");
		return "/welcome";//request handling controller returns :logical view name(forword view)
	  //to D.S (DispatcherServlet) 
	}
	
	//add req handling method to test o.s.w.s.ModelAndView
	@RequestMapping("/hello2")
	public ModelAndView sayHello2()
	{
		System.out.println("in say hello2");
		//o.s.w.s.ModelAndView :holder for holding ModelAtrribute + logical view name
		//constructor ModelAndView(String logicalViewName,String ModelAttName,Object modelAttValue)
		//def scope of model attr: current request only
		
		return new ModelAndView("/welcome","server_timestamp",LocalDateTime.now());
		//req handling controller is returning --->logical view name + 1 model attribute to the dispatcher servlet
	}
	
	//add req handling method to test Model Map
	//o.s.ui.Model :i/f => holder of model attributes
	//How to add attributes? MOdel addAttribute(String modelAttrName,Object modelAttrVal)
	//IoC : simply tell SC : to inject EMPTY model map in the req handling method : D.I : by adding an 
	//argument to req handling method
	@RequestMapping("/hello3")
	public String sayHello3(Model map)
	{
		System.out.println("in say hello3 "+map);//{}
		//add 2 model attributes
		map.addAttribute("server_timestamp",LocalDateTime.now())
		.addAttribute("num_list", Arrays.asList(10,20,30));
		System.out.println("in say hello3 After "+map);//populated map
		return "/welcome";//Req handling controller(=handler) explicitly only rets logical view name.
		//BUT implicitly it rets additionally model attrs stored under Model map
	}
}

