package dependent;


import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import dependency.HttpTransport;
import dependency.Transport;
//singleton n eager,id=my_atm
@Component("my_atm")//to tell SC whatever follows is a spring bean : whose life cycle has to be managed by SC
public class ATMImpl implements ATM {
	@Autowired//(required=false)//required=true=>mandatory,autowire=byType
	@Qualifier("httpTransport")//autowire=byName
	//SC tries to match data type of the property with data type of dependency bean
	private Transport myTransport;
	public ATMImpl() {		
		System.out.println("in cnstr of " +getClass().getName()+" "+myTransport);
	}

	@Override
	public void deposit(double amt) {
		System.out.println("depositing "+amt);
		byte[] data=("depositing "+amt).getBytes();
		myTransport.informBank(data);
	}

	@Override
	public void withdraw(double amt) {
		System.out.println("withdrawing "+amt);
		byte[] data=("withdrawing "+amt).getBytes();
		myTransport.informBank(data);
	}

	//init style method
	@PostConstruct
	public void myInit()
	{
		System.out.println("in my init of "+getClass().getName()+" dependency "+myTransport);
	}
	//destroy style method
	@PreDestroy
	public void myDestroy()
	{
		System.out.println("in my destroy of "+getClass().getName()+" dependency "+myTransport);
	}
	
	
}
