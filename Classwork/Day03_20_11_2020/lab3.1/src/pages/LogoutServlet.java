package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookDaoImplementation;
import pojos.Book;
import pojos.Customer;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			// get HttpSession from WC
			HttpSession session = request.getSession();
			// new / old
			System.out.println("From logout page HS " + session.isNew());// false
			System.out.println("Session Id " + session.getId());// SAME
			// retrieve customer details from session scope
			Customer c = (Customer) session.getAttribute("user_details");
			if (c != null) {
				pw.print("<h5> Hello " + c.getEmail() + "</h5>");
				// retrieve cart from session scope
				List<Integer> cart = (List<Integer>) session.getAttribute("shopping_cart");
				// retrieve book dao from session
				BookDaoImplementation bookDao = (BookDaoImplementation) session.getAttribute("book_dao");
				// invoke book dao's method : to fetch actual bk details from DB (via dao)
				pw.print("<h4>Cart Contents</h4>");
				double totalCartValue=0;
				for (int id : cart) {
					// get book details
					Book book = bookDao.getBookDetails(id);
					//send book details to clnt : PW
					pw.print("<h5>"+book+"</h5>");
					totalCartValue += book.getPrice();
				}
				pw.print("<h5> Total Cart Value "+totalCartValue+"</h5>");
				

			} else
				pw.print("<h5> No Cookies , Session Tracking failed!!!!!</h5>");
			// invalidate HttpSession
			session.invalidate();

			pw.print("<h5>You have logged out....</h5>");
			// send a link for user to visit again
			pw.print("<h5><a href='login.html'>Visit Again</a></h5>");
		} catch (Exception e) {
			throw new ServletException("err in do-get of "+getClass().getName(), e);
		}
	}
}
