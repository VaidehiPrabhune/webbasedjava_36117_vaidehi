package com.app.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Role;
import com.app.pojos.Vendor;

@Repository
public class VendorDaoImpl implements IVendorDao {
	//DAO :dependent object, dependency :sSessionFactory
	@Autowired //autowire=byType
	private SessionFactory factory;
	
	
	@Override
	public Vendor authenticateUser(String email, String password) {
		Vendor v = null;
		String jpql = "select v from Vendor v  where v.email=:em and v.password=:pass";				
		v = factory.getCurrentSession().createQuery(jpql, Vendor.class).setParameter("em", email).
			setParameter("pass", password)
			.getSingleResult();
			
		return v;//dao layer is returning PERSISTENT vendor pojo to service
	}


	@Override
	public List<Vendor> listAllVendors() {
		String jpql="select v from Vendor v where v.userRole=:role";		
		return factory.getCurrentSession().createQuery(jpql, Vendor.class).setParameter("role", Role.VENDOR).getResultList();
	}

}
