package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.service.IVendorService;

@Controller
@RequestMapping("/admin")
public class AdminController {
	//dependancy :vendor service layer
	@Autowired
	private IVendorService vendorService;
	
	public AdminController() {
		System.out.println("in ctor of "+getClass().getName());
	}
	//add request handling method to show vendor list
	@GetMapping("/list")
	public String showVendorList(Model map)
	{
		System.out.println("in show vendor list"+map);
		//save current vendor list under model map -> auto saved under current req scope
		map.addAttribute("vendor_list",vendorService.listAllVendors());
		return "/admin/list";// Forward view name from D.S to view resolver
		//actual view name return by view resolver :/WEB-INF/views/vendor/list.jsp
	}
	
	
}
