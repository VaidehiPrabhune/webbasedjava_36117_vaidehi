package com.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/vendor")
public class VendorController {
	public VendorController() {
		System.out.println("in ctor of "+getClass().getName());
	}
	
	//add a request handling method to show vendor details
	@GetMapping("/details")
	public String showVendorDetails()
	{
		System.out.println("in show vendor details");
		return "/vendor/details";///we can write like this as well vendor/vendor123 
		//actual view name :/WEB-INF/views/vendor/details
	}
}
