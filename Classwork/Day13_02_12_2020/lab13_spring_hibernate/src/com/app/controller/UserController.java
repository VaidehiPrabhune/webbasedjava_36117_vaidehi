package com.app.controller;

import org.springframework.ui.Model;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.pojos.Role;
import com.app.pojos.Vendor;
import com.app.service.IVendorService;

@Controller // mandatory
@RequestMapping("/user") // optional but recommended
public class UserController {
	// dependency :service layer
	@Autowired
	private IVendorService vendorService;

	public UserController() {
		System.out.println("in ctor of " + getClass().getName());
	}

	@PostConstruct
	public void init()
	{
		System.out.println("in user controller init "+vendorService);//not null
	}
	// add request handling method : get
	@GetMapping("/login")
	public String showLoginForm() {
		System.out.println("in show login form ");
		return "/user/login";// actual view name :/WEB-INF/views/user/login.jsp
	}

	// add req handling method with method=post : to process the form
	@PostMapping("/login")
	public String processLoginForm(@RequestParam String email, @RequestParam String password, Model map,HttpSession session) {
		System.out.println("in process login form " + email + " " + password);
		// invoke service layer method for exec B.L
		try {
			Vendor validatedUser = vendorService.authenticateUser(email, password);
			//=>valid login
			//add validated user deatils under session scope
			session.setAttribute("user_details", validatedUser);
			//chk role n redirect accordingly
			if(validatedUser.getUserRole().equals(Role.ADMIN))
				return "redirect:/admin/list";
			//controller sends redirect view name to D.S 
			//D.S : skips the V.R & invokes : 
			//response.sendRedirect(response.encodeRedirectURL("/admin/list"));
			//WC : sends redirect resp pkt : SC 302 | location : /admin/list + jsessionid | body : empty
			//clnt brower sends next request : http://host:port/day13/admin/list;jsessionid=g7567gh,
			//method=get
			
			return "redirect:/vendor/details";//if no redirect then it will send in the same request and to avoid double submit issue
			//replace server pull by client pull
		
		} catch (RuntimeException e) {
			System.out.println("err in process login form " + e);// NoResultExc
			// in case of failure : FORWARD client to login form : highlighted with error
			// message
			map.addAttribute("message", "Invalid Login , Pls retry....");
			return "/user/login";// actual view name : /WEB-INF/views/user/login.jsp

		}		
	}

}
