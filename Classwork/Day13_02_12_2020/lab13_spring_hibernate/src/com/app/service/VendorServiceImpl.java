package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IVendorDao;
import com.app.pojos.Vendor;

@Service //mandatory annotation to tell SC whatever follows contains B.L 
@Transactional//mandatory : annotation to tell SC to use transaction mgr bean
//for automatically handling transactions
public class VendorServiceImpl implements IVendorService {
	//dependency :DAO layer
	@Autowired
	private IVendorDao vendorDao;
	
	@Override
	public Vendor authenticateUser(String email, String password) {
		//simply invoke dao's method for user authentication
		return vendorDao.authenticateUser(email, password);	
	}

	@Override
	public List<Vendor> listAllVendors() {
		
		return vendorDao.listAllVendors();
	}

}
