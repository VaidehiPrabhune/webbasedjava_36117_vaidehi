<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h5>Admin login successful....</h5>
	<h5>Admin's details :${sessionScope.user_details}</h5>
		
	<table style="background-color: cyan; margin: auto;" border="1">
		<caption>Vendor List</caption>
		<c:forEach var="v" items="${requestScope.vendor_list}">
			<tr>
				<td>${v.name}</td>
				<td>${v.email}</td>
				<td>${v.regAmount}</td>
				<td>${v.regDate}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>