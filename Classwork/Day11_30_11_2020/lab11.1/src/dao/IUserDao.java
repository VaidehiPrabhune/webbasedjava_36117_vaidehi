package dao;

import java.util.Date;
import java.util.List;

import pojos.Role;
import pojos.User;

public interface IUserDao {
//add a method for user registration
	String registerUser(User user);

	// add a method to fetch user details by its id
	User fetchUserDetails(int userId);

	// add a method to fetch details of all users
	List<User> fetchAllUserDetails();

	// add method to fetch selected user details
	List<User> fetchSelectedUserDetails(Date strtDate, Date endDate, Role userRole);

	// Objective : Display all user names registered between strt date n end date &
	// under a specific role
	List<String> fetchSelectedUserNames(Date strtDate, Date endDate, Role userRole);
	// Display all user names,reg amount,reg date registered between strt date n end
	// date & under a specific role
	List<User> fetchSelectedDetails(Date strtDate, Date endDate, Role userRole);
	//Change password
	//i/p --user email ,old password , new pass
	String changePassword(String email,String oldPwd,String newPwd);
	//add method to  Un subscribe user : i/p  : email n password
	String unsubscribeUser(String email,String password);
	// Apply discount to reg amount , for all users , reged before a specific date. (via bulk update)
	String bulkUpdateUsers(Date date, double discount);
	//add method to understand save vs persist vs saveOrUpdate 
	String testSessionAPI(User user);
	//add method to store binary image for existing user
	//inputs : user id , file 
	String saveImage(int userId,String fileName) throws Exception;
	//add method to restore binary image for existing user into specified o/p file name
	String restoreImage(int userId,String fileName) throws Exception;
}
