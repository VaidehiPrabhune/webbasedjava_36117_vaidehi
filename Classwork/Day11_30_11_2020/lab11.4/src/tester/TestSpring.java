package tester;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import dependent.ATMImpl;

public class TestSpring {

	public static void main(String[] args) {
		//start spring container :using xml based meta data instructions,placed in run time claspath
		//os.c.s.ClassPthXmlApplicationContext :class
		//BeanFactory <----- ApplicationContext <-----ClassPathXmlApplicationContext
		try(ClassPathXmlApplicationContext ctx=new ClassPathXmlApplicationContext("config.xml"))
		{
			System.out.println("SC booted...");
			//1st demand:tell SC to supply located--loaded-instantiated(default const)--Dependancy injected=>ready to use bean
			//API :o.s.b.BeanFactory :T getBean(String beanId,Class<T> beanClass) throws BeanException
			System.out.println("making 1st demand");
			ATMImpl atmBean1=ctx.getBean("atm_bean",ATMImpl.class);
			//B.L
			atmBean1.withdraw(1234);;
			System.out.println("making 2nd demand");
			ATMImpl atmBean2=ctx.getBean("atm_bean",ATMImpl.class);
			ATMImpl atmBean3=ctx.getBean("atm_bean",ATMImpl.class);
			System.out.println(atmBean1 == atmBean2);//true(ref equality check)
			System.out.println(atmBean1 == atmBean3);//true(ref equality check)
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}

	}

}
