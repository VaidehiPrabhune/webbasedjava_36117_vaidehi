package dependency;

import java.time.LocalDateTime;

public class SMSNotification implements NotificationService {

	public SMSNotification() {
		System.out.println("in ctor of "+getClass().getName());
	}
	@Override
	public void notifyCustomer(String txType, double amount) {
		System.out.println("Notifyning customer :Tx type "+txType+"for amount= "+amount
		+" @ "+LocalDateTime.now()+" via sms");
	}

}
