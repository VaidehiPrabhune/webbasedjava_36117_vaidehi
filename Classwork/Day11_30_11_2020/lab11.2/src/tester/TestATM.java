package tester;

import dependency.SoapTransport;
import dependent.ATMImpl;

public class TestATM {
	public static void main(String[] args) {
		//create dependent object,create dependency, n inject dependency/ies invoke B.L 
		ATMImpl atm=new ATMImpl();
		atm.setMyTransport(new SoapTransport());
		atm.deposit(1000);
	}
}
