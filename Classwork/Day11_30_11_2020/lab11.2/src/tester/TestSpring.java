package tester;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import dependent.ATMImpl;

public class TestSpring {

	public static void main(String[] args) {
		//start spring container :using xml based meta data instructions,placed in run time classpath
		//class :o.s.c.s.ClassPathXmlApplicationContext(String configFile) throws BeanException
		try(ClassPathXmlApplicationContext ctx=new ClassPathXmlApplicationContext("config.xml"))
		{
			System.out.println("SC started...");
			//get ready made spring bean instance from S.C, for invoking B.L
			System.out.println("making 1st demand");
			ATMImpl atmBean1=ctx.getBean("my_atm",ATMImpl.class);
			//B.L
			atmBean1.deposit(1234);
			System.out.println("making 2nd demand");
			ATMImpl atmBean2=ctx.getBean("my_atm",ATMImpl.class);
			System.out.println(atmBean1 == atmBean2);//true
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}

	}

}
