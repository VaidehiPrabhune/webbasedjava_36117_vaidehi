<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%--save product details under session scope without using a scriplet --%>
	<%--pid=123&price=2000&category=book --%>
	<c:set var="product_dtls"
		value="${param.pid} : ${param.price} : ${param.category}"
		scope="session" />
	<%
		//use URL Rewriting : method of HttpServletResponse : encodeRedirectURL
		String encodedURL = response.encodeRedirectURL("test2.jsp");
		response.sendRedirect(encodedURL);
	%>



</body>
</html>