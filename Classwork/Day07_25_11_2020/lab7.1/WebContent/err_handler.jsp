<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h5>Error Causing Page :${pageContext.errorData.requestURI}</h5>
	<h5>Error Message :${pageContext.exception.message}</h5>
	<h5>Error Status Code : ${pageContext.errorData.statusCode}</h5>
	<h5>Error Details :${pageContext.errorData.throwable}</h5>
	<h5>
		Via Expression Tag Error Details :
		<%=exception%></h5>
</body>
</html>