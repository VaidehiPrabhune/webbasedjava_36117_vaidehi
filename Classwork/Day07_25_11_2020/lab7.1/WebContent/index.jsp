<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h5>
		<a href="test1.jsp?pid=123&price=2000&category=book">Test URL
			rewriting Client Pull I</a>
	</h5>
	<h5>
		<a href="test3.jsp?pid=123&price=2000&category=book">Test URL
			rewriting Client Pull II</a>
	</h5>
	<h5>
		<a href="test4.jsp?pid=123&price=2000&category=book">Test URL
			rewriting Client Pull I Using JSTL</a>
	</h5>
	<h5>
		<a href="test5.jsp?pid=123&price=2000&category=book">Test URL
			rewriting Client Pull II Using JSTL</a>
	</h5>
	<h5><a href="test6.jsp">Test Centralized Error Handling in JSP</a></h5>
	
	<h5>
	<a href="test7.jsp">Test include directive</a>
	</h5>
</body>
</html>