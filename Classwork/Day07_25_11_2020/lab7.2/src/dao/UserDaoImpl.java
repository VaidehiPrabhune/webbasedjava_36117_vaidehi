package dao;
import pojos.User;
import static utils.HibernateUtils.getSf;
import org.hibernate.*;
//Below is native hibernate based DAO layer.(completely hibernate specific)

public class UserDaoImpl implements IUserDao{

	@Override
	public String registerUser(User user) {
		String message="User registration failed..";
		//1.get hibernate Session from SessionFactory : openSession
		Session hibSession =getSf().openSession();
		//2.begin a transaction(tx)
		Transaction tx=hibSession.beginTransaction();
		//L1 cache is created in empty manner
		try {
			hibSession.save(user);
			//success.commit
			tx.commit();
			message ="User registered successfully with ID"+user.getUserId();
		}catch(HibernateException e) {
			if(tx != null)
				tx.rollback();
			//inform /alert the caller about exception : re throw the SAME exception to caller
			throw e;
		}finally {
			if(hibSession != null)
				hibSession.close();//L1 cache is destroyed n pooled out DB connection pool,
			//so that SAME DB connection can be reused for another request.
		}
		return null;
	}

}
