package tester;
import static utils.HibernateUtils.getSf;

import java.text.SimpleDateFormat;
import java.util.Scanner;

import org.hibernate.*;

import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;

public class RegisterNewUser {

	public static void main(String[] args) {
		// Testing bootstrapping of hibernate configuration (creating singleton n
		// immutable instance of SessionFactory (SF)
		//to parse string -->Date
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		try(SessionFactory sf=getSf();Scanner sc=new Scanner(System.in))
		{
			System.out.println("Enter user details: name,email,password,confirmPassword role regDate");
			//create a transient POJO (not yet persistent)
			User u1=new User(sc.next(), sc.next(), sc.next(), sc.next(),Role.valueOf(sc.next().toUpperCase()) , sc.nextDouble(),sdf.parse(sc.next()) );//exist in java heap
			//create dao instance n invoke method
			UserDaoImpl dao=new UserDaoImpl();
			System.out.println("Reg status"+dao.registerUser(u1));
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
